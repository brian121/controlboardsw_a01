/* ###################################################################
**     This component module is generated by Processor Expert. Do not modify it.
**     Filename    : bctu1.c
**     Project     : ControlBoardSW_A01_Z4_1
**     Processor   : MPC5745R_176
**     Component   : bctu
**     Version     : Component S32_SDK_C55, Driver 01.00, CPU db: 3.00.000
**     Repository  : SDK_S32_PA_08
**     Compiler    : GNU C Compiler
**     Date/Time   : 2018-12-28, 03:28, # CodeGen: 2
**
**     Copyright 1997 - 2015 Freescale Semiconductor, Inc.
**     Copyright 2016-2017 NXP
**     All Rights Reserved.
**     
**     THIS SOFTWARE IS PROVIDED BY NXP "AS IS" AND ANY EXPRESSED OR
**     IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
**     OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**     IN NO EVENT SHALL NXP OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
**     INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
**     (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
**     SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
**     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
**     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
**     IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
**     THE POSSIBILITY OF SUCH DAMAGE.
** ###################################################################*/
/*!
** @file bctu1.c
** @version 01.00
*/
/*!
**  @addtogroup bctu1_module bctu1 module documentation
**  @{
*/

/* MODULE bctu1. */

/**
 * @page misra_violations MISRA-C:2012 violations
 *
 * @section [global]
 * Violates MISRA 2012 Advisory Rule 8.7, External could be made static.
 * Function is defined for usage by application code.
 */

#include "bctu1.h"

/*! bctu1 configuration structures */

const bctu_config_t bctu1_globalConfig0 = {
  .lowPowerModeEn = false,
  .globalHwTriggersEn = true,
  .globalTriggersEn = true,
  .dmaEnMask = 7U,
  .triggerIntEn = false,
  .listIntEn = true,
  .newDataIntEnMask = 0U,
};

const bctu_trig_config_t bctu1_trigConfig0 = {
  .loopEn = false,
  .adcTargetMask = 7U,
  .hwTriggersEn = false,
  .dest2PSI = false,
  .ccp = 0U,
  .tag = 0U,
};

bctu_adc_chan_tag_t bctu1_listConfig0_array[33U] ={ \
/* adc channel, PSI tag */
          { 0U, 0U }, \
          { 0U, 0U }, \
          { 8U, 0U }, \
          { 2U, 0U }, \
          { 2U, 0U }, \
          { 10U, 0U }, \
          { 3U, 0U }, \
          { 3U, 0U }, \
          { 11U, 0U }, \
          { 4U, 0U }, \
          { 5U, 0U }, \
          { 12U, 0U }, \
          { 5U, 0U }, \
          { 6U, 0U }, \
          { 13U, 0U }, \
          { 6U, 0U }, \
          { 9U, 0U }, \
          { 14U, 0U }, \
          { 7U, 0U }, \
          { 11U, 0U }, \
          { 15U, 0U }, \
          { 7U, 0U }, \
          { 13U, 0U }, \
          { 16U, 0U }, \
          { 7U, 0U }, \
          { 14U, 0U }, \
          { 17U, 0U }, \
          { 7U, 0U }, \
          { 15U, 0U }, \
          { 18U, 0U }, \
          { 7U, 0U }, \
          { 17U, 0U }, \
          { 18U, 0U }, \
};

const bctu_conv_list_t bctu1_listConfig0 = {
    .startElemIdx = 0U,
    .numArrayElems = 33U,
    .adcChanTag = bctu1_listConfig0_array,
};

/* END bctu1. */
/*!
** @}
*/
/*
** ###################################################################
**
**     This file was created by Processor Expert 10.1 [05.21]
**     for the NXP C55 series of microcontrollers.
**
** ###################################################################
*/

