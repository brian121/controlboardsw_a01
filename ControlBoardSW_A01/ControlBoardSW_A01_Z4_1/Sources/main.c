/*
 * Copyright (c) 2013 - 2015, Freescale Semiconductor, Inc.
 * Copyright 2016-2017 NXP
 * All rights reserved.
 *
 * THIS SOFTWARE IS PROVIDED BY NXP "AS IS" AND ANY EXPRESSED OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL NXP OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* ###################################################################
**     Filename    : main.c
**     Processor   : MPC574xR
**     Abstract    :
**         Main module.
**         This module contains user's application code.
**     Settings    :
**     Contents    :
**         No public methods
**
** ###################################################################*/
/*!
** @file main.c
** @version 01.00
** @brief
**         Main module.
**         This module contains user's application code.
*/
/*!
**  @addtogroup main_module main module documentation
**  @{
*/
/* MODULE main */


/* Including necessary module. Cpu.h contains other modules needed for compiling.*/
#include "Cpu.h"
#include "clockMan1.h"
#include "VCAN.h"
#include "DCAN.h"
#include "dmaController1.h"
#include "pin_mux.h"
#if CPU_INIT_CONFIG
  #include "Init_Config.h"
#endif

/* User includes (#include below this line is not maintained by Processor Expert) */
#include <stdint.h>
#include <stdbool.h>

#define LED_PORT        PTA
#define LED0            0U
#define LED1            3U

#define BTN_PORT        PTD
#define BTN0_PIN        0U
#define BTN1_PIN        1U
#define BTN0_EIRQ       0U
#define BTN1_EIRQ       1U

/* Definition of the TX and RX message buffers depending on the bus role */

#define TX_MAILBOX  (1UL)
//ANP2 when using rxfifo and 8 entry filter table, the first 7 MB's are occupied, so transmit w MB 8
#define TX_MAILBOX_RXFIFO_8  (8UL)
#define TX_MSG_ID1  (1UL)
#define TX_MSG_ID2  (2UL)
#define RX_MAILBOX  (0UL)
#define RX_MSG_ID1  (2UL)
#define RX_MSG_ID2  (1UL)

#define MAX_CAN_BUFFER_SIZE  (64UL) //ANP2 CAN_driver.h data buffer size is 64Bytes

typedef enum
{
    LED0_CHANGE_REQUESTED = 0x00U,
    LED1_CHANGE_REQUESTED = 0x01U
} can_commands_list;

uint8_t ledRequested = (uint8_t)LED0_CHANGE_REQUESTED;  //ANP2  1 byte transfer

uint8_t one_hundred_bytes[100];

volatile int exit_code = 0;

/******************************************************************************
 * Function prototypes
 ******************************************************************************/
void SendCANData(uint8_t instance, uint32_t mailbox, uint32_t messageId, uint8_t * data, uint32_t len);
void buttonISR(void);
void BoardInit(void);
void GPIOInit(void);

/******************************************************************************
 * Functions
 ******************************************************************************/

/**
 * Button interrupt handler
 */
void buttonISR(void)
{
    /* Check if one of the buttons was pressed */
    uint32_t button0 = PINS_DRV_GetPinExIntFlag(BTN0_EIRQ);
    uint32_t button1 = PINS_DRV_GetPinExIntFlag(BTN1_EIRQ);

    /* Set FlexCAN TX value according to the button pressed */
    if (button0 != 0)
    {
        ledRequested = LED0_CHANGE_REQUESTED;
        /* Send the information via CAN */
        //ANP2 use MB for TX that allows for rxfifo & filter MB offset
        //SendCANData(INST_CANCOM1,TX_MAILBOX, TX_MSG_ID1, &ledRequested, 1UL);
#define SEND_100  //ANP2 CAN driver data buffer size is 64Bytes
#ifdef SEND_100
        SendCANData(INST_VCAN,TX_MAILBOX_RXFIFO_8, TX_MSG_ID1, &one_hundred_bytes[0], MAX_CAN_BUFFER_SIZE);
#else
        SendCANData(INST_VCAN,TX_MAILBOX_RXFIFO_8, TX_MSG_ID1, &ledRequested, 1UL);
#endif

        /* Clear interrupt flag */
        PINS_DRV_ClearPinExIntFlag(BTN0_EIRQ);
    }
    else if (button1 != 0)
    {
        ledRequested = LED1_CHANGE_REQUESTED;
        /* Send the information via CAN */
        SendCANData(INST_DCAN,TX_MAILBOX, TX_MSG_ID2, &ledRequested, 1UL);
        /* Clear interrupt flag */
        PINS_DRV_ClearPinExIntFlag(BTN1_EIRQ);
    }
    else
    {
        PINS_DRV_ClearExIntFlag();
    }

}

/*
 * @brief: Send data via CAN to the specified mailbox with the specified message id
 * @param mailbox   : Destination mailbox number
 * @param messageId : Message ID
 * @param data      : Pointer to the TX data
 * @param len       : Length of the TX data
 * @return          : None
 */
void SendCANData(uint8_t instance, uint32_t mailbox, uint32_t messageId, uint8_t * data, uint32_t len)
{
    /* Set information about the data to be sent
     *  - 1 byte in length
     *  - Standard message ID
     *  - Bit rate switch disabled
     *  - Flexible data rate disabled
     *  - Use zeros for FD padding
     */
    flexcan_data_info_t dataInfo =
    {
            .data_length = len,
            .msg_id_type = FLEXCAN_MSG_ID_STD
    };

    /* Configure TX message buffer with index TX_MSG_ID1 and TX_MAILBOX*/
    FLEXCAN_DRV_ConfigTxMb(instance, mailbox, &dataInfo, messageId);

    /* Execute send non-blocking */
    FLEXCAN_DRV_Send(instance, mailbox, &dataInfo, messageId, data);
}

/*
 * @brief : Initialize clocks, pins and power modes
 */
void BoardInit(void)
{

    /* Initialize and configure clocks
     *  -   Setup system clocks, dividers
     *  -   Configure FlexCAN clock, GPIO
     *  -   see clock manager component for more details
     */
    CLOCK_SYS_Init(g_clockManConfigsArr, CLOCK_MANAGER_CONFIG_CNT,
                        g_clockManCallbacksArr, CLOCK_MANAGER_CALLBACK_CNT);
    //CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_FORCIBLE); //ANP2 20Mhz XTAL config
    CLOCK_SYS_UpdateConfiguration(1U, CLOCK_MANAGER_POLICY_FORCIBLE); //ANP2 25Mhz XTAL config

    /* Initialize pins
     *  -   Init FlexCAN and GPIO pins
     *  -   See PinSettings component for more info
     */
    PINS_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);
}

/*
 * @brief Function which configures the LEDs and Buttons
 */
void GPIOInit(void)
{

	siul2_interrupt_config_t   btn0_cfg_irq = {
			.eirqPinIdx = BTN0_EIRQ,
			.intEdgeSel = SIUL2_INT_RISING_EDGE,
			.digitalFilter = false,
			.maxCnt = 0U,
			.intExeSel = SIUL2_INT_USING_INTERUPT
	};

	siul2_interrupt_config_t   btn1_cfg_irq = {
			.eirqPinIdx = BTN1_EIRQ,
			.intEdgeSel = SIUL2_INT_RISING_EDGE,
			.digitalFilter = false,
			.maxCnt = 0U,
			.intExeSel = SIUL2_INT_USING_INTERUPT
		};

    /* Set Output value LEDs */
    PINS_DRV_SetPins(LED_PORT, (1 << LED0) | (1 << LED1));

    /* Configure Interrupts Requests For Button 1 and Button 2 */
    PINS_DRV_SetExInt(btn0_cfg_irq);
    PINS_DRV_SetExInt(btn1_cfg_irq);

    /* Install buttons ISR */
    INT_SYS_InstallHandler(SIU_0_IRQn, &buttonISR, NULL);

    /* Enable buttons interrupt */
    INT_SYS_EnableIRQ(SIU_0_IRQn);
}

/*!
  \brief The main function for the project.
  \details The startup initialization sequence is the following:
 * - startup asm routine
 * - main()
*/
int main(void)
{
  /* Write your local variable definition here */

  /*** Processor Expert internal initialization. DON'T REMOVE THIS CODE!!! ***/
  #ifdef PEX_RTOS_INIT
    PEX_RTOS_INIT();                   /* Initialization of the selected RTOS. Macro is defined by the RTOS component. */
  #endif
  /*** End of Processor Expert internal initialization.                    ***/

    status_t eDMA_status; //ANP2 status of dma init
    edma_state_t edmaState;

    /* Do the initializations required for this application */
    BoardInit();
    GPIOInit();

    /** Description   : Initialize the eDMA module.   */
    eDMA_status = EDMA_DRV_Init(&edmaState, &dmaController1_InitConfig0, edmaChnStateArray,  edmaChnConfigArray, EDMA_CONFIGURED_CHANNELS_COUNT);
    //DevAssert(eDMA_status == STATUS_SUCCESS);

#ifdef SEND_100
    //ANP2 init send buffer
	for (int i=0; i<100; i++)
    {
		one_hundred_bytes[i] = i+1;
	}
#endif

    /*
     * Initialize FlexCAN driver
     *  - 8 byte payload size
     *  - FD disabled
     *  - Oscillator clock as peripheral engine clock
     */
    FLEXCAN_DRV_Init(INST_VCAN, &canCom1_State, &canCom1_InitConfig0);
    FLEXCAN_DRV_Init(INST_DCAN, &canCom2_State, &canCom2_InitConfig0);
    /* Set information about the data to be received
     *  - 1 byte in length
     *  - Standard message ID
     *  - Bit rate switch disabled
     *  - Flexible data rate disabled
     *  - Use zeros for FD padding
     */
    flexcan_data_info_t dataInfo =
    {
#ifdef SEND_100
            .data_length = MAX_CAN_BUFFER_SIZE,
#else
            .data_length = 1U,
#endif
            .msg_id_type = FLEXCAN_MSG_ID_STD
    };

    /* Configure RX message buffer with index RX_MSG_ID and RX_MAILBOX */
    //ANP2 use FIFO mode on master w/ DMA for Pierburg
    //FLEXCAN_DRV_ConfigRxMb(INST_CANCOM1, RX_MAILBOX, &dataInfo, RX_MSG_ID1);
    /* ID Filter table */
    //ANP2 configure all 8 filter elements.  All must be filled for proper configuration
    const flexcan_id_table_t filterTable[] = {
    		{
    		.isExtendedFrame = false,
			.isRemoteFrame = false,
			.id = RX_MSG_ID1
    		},
    		{
    		.isExtendedFrame = false,
			.isRemoteFrame = false,
			.id = RX_MSG_ID1
    		},
    		{
    		.isExtendedFrame = false,
			.isRemoteFrame = false,
			.id = RX_MSG_ID1
    		},
    		{
    		.isExtendedFrame = false,
			.isRemoteFrame = false,
			.id = RX_MSG_ID1
    		},
    		{
    		.isExtendedFrame = false,
			.isRemoteFrame = false,
			.id = RX_MSG_ID1
    		},
    		{
    		.isExtendedFrame = false,
			.isRemoteFrame = false,
			.id = RX_MSG_ID1
    		},
    		{
    		.isExtendedFrame = false,
			.isRemoteFrame = false,
			.id = RX_MSG_ID1
    		},
    		{
    		.isExtendedFrame = false,
			.isRemoteFrame = false,
			.id = RX_MSG_ID1
    		}


    };
    FLEXCAN_DRV_ConfigRxFifo(INST_VCAN, FLEXCAN_RX_FIFO_ID_FORMAT_A, filterTable);

    FLEXCAN_DRV_ConfigRxMb(INST_DCAN, RX_MAILBOX, &dataInfo, RX_MSG_ID2);

    while(1)
    {
        /* Define receive buffer */
        flexcan_msgbuff_t recvBuff, recvBuff1;

        recvBuff.msgId=0U;
        recvBuff1.msgId=0U;
        /* Start receiving data in RX_MAILBOX. */
        //ANP2 use FIFO mode on master w/ DMA for Pierburg
        //FLEXCAN_DRV_Receive(INST_CANCOM1, RX_MAILBOX, &recvBuff);
        FLEXCAN_DRV_RxFifo(INST_VCAN, &recvBuff);
        FLEXCAN_DRV_Receive(INST_DCAN, RX_MAILBOX, &recvBuff1);

        /* Wait until the previous FlexCAN receive is completed */
        while((FLEXCAN_DRV_GetTransferStatus(INST_VCAN, RX_MAILBOX) == STATUS_BUSY) &&
        		(FLEXCAN_DRV_GetTransferStatus(INST_DCAN, RX_MAILBOX) == STATUS_BUSY));

        /* Check the received message ID and payload */
        if(recvBuff.msgId == RX_MSG_ID1)
        {
            /* Toggle output value LED0 */
            PINS_DRV_TogglePins(LED_PORT, (1 << LED0));

        }
        else if(recvBuff1.msgId == RX_MSG_ID2)
        {
            /* Toggle output value LED1 */
            PINS_DRV_TogglePins(LED_PORT, (1 << LED1));
        }
    }

  /*** Don't write any code pass this line, or it will be deleted during code generation. ***/
  /*** RTOS startup code. Macro PEX_RTOS_START is defined by the RTOS component. DON'T MODIFY THIS CODE!!! ***/
  #ifdef PEX_RTOS_START
    PEX_RTOS_START();                  /* Startup of the selected RTOS. Macro is defined by the RTOS component. */
  #endif
  /*** End of RTOS startup code.  ***/
  /*** Processor Expert end of main routine. DON'T MODIFY THIS CODE!!! ***/
  for(;;) {
    if(exit_code != 0) {
      break;
    }
  }
  return exit_code;
  /*** Processor Expert end of main routine. DON'T WRITE CODE BELOW!!! ***/
} /*** End of main routine. DO NOT MODIFY THIS TEXT!!! ***/

/* END main */
/*!
** @}
*/
/*
** ###################################################################
**
**     This file was created by Processor Expert 10.1 [05.21]
**     for the NXP C55 series of microcontrollers.
**
** ###################################################################
*/
