/*
** ###################################################################
**
**     Abstract:
**         Chip specific module features.
**
**     Copyright 2018 NXP
**     All rights reserved.
**
**     THIS SOFTWARE IS PROVIDED BY NXP "AS IS" AND ANY EXPRESSED OR
**     IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
**     OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**     IN NO EVENT SHALL NXP OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
**     INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
**     (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
**     SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
**     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
**     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
**     IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
**     THE POSSIBILITY OF SUCH DAMAGE.
**
**
** ###################################################################
*/

/*!
 * @file MPC5745R_features.h
 *
 * @page misra_violations MISRA-C:2012 violations
 *
 * @section [global]
 * Violates MISRA 2012 Advisory Rule 2.3, Global typedef not referenced.
 * Type used only in some modules of the SDK.
 *
 * @section [global]
 * Violates MISRA 2012 Advisory Rule 2.5, Global macro not referenced.
 * The macros defined are used to define features for each driver, so this might be reported
 * when the analysis is made only on one driver.
 *
 * @section [global]
 * Violates MISRA 2012 Advisory Directive 4.9, Function-like macro
 * These are very simple macros used for abstracting hw implementation.
 * They help make the code easy to understand.
 *
 * @section [global]
 * Violates MISRA 2012 Required Rule 5.1, identifier clash
 * The supported compilers use more than 31 significant characters for identifiers.
 *
 * @section [global]
 * Violates MISRA 2012 Required Rule 5.2, identifier clash
 * The supported compilers use more than 31 significant characters for identifiers.
 *
 * @section [global]
 * Violates MISRA 2012 Required Rule 5.4, identifier clash
 * The supported compilers use more than 31 significant characters for identifiers.
 *
 * @section [global]
 * Violates MISRA 2012 Required Rule 5.5, identifier clash
 * The supported compilers use more than 31 significant characters for identifiers.
 *
 */

#if !defined(MPC5745R_FEATURES_H)
#define MPC5745R_FEATURES_H

/* include device_registers.h for SIUL2 module configuration structures */
#include "device_registers.h"

/* ERRATA sections*/

/* ERRATA sections*/

/* @brief E8759: FlexCAN: FD frame format is not compliant to the new ISO/CD 11898-1: 2014-12-11
 * because CAN_CTRL2[ISOCANFDEN] bit cannot be set. The FD frame format is incompatible, the Cyclic
 * Redundancy Check [CRC] does not include the added stuff bit count field. The FD CRC computation
 * is incompatible, a different seed value is used.
 * This errata applies to mask 1N81M for MPC5746R CPU.
 */
#define ERRATA_E8759

/* @brief E8341 FlexCAN: Entering Freeze Mode or Low Power Mode from Normal Mode
 * can cause the module to stop operating
 * In the Flexible Controller Area Network (FlexCAN) module, if the Freeze Enable bit (FRZ) in
 * the Module Configuration Register (MCR) is asserted and the Freeze Mode is requested by
 * asserting the Halt bit (HALT) in MCR, in some cases, the Freeze Mode Acknowledge bit
 * (FRZACK) in the MCR may never be asserted.
 * In addition, the Low-Power Mode Acknowledge bit (LPMACK) in the MCR may never be
 * asserted in some cases when the Low-Power Mode is requested
 * Under the two scenarios described above, the loss of ACK assertion (FRZACK, LPMACK) causes a
 * lock condition. A soft reset action is required in order to remove the lock condition.
 * The change from Normal Mode to Low-Power Mode cannot be done directly. Instead, first
 * change mode from Normal to Freeze Mode, and then from Freeze to Low-Power Mode.
 * This errata applies to mask 1N81M for MPC5746R CPU.
 */
#define ERRATA_E8341

/* Mode Entry Module features */

/* @brief Key and inverted key values so write access to MCTL register is permitted */
#define FEATURE_MC_ME_KEY           (0x5AF0U)
#define FEATURE_MC_ME_KEY_INV       (0xA50FU)
/* @brief Mode entry has TEST mode */
#define FEATURE_MC_ME_HAS_TEST_MODE (1U)
/* @brief Mode entry has HALT mode */
#define FEATURE_MC_ME_HAS_HALT_MODE (1U)
/* @brief Mode entry has STOP0 mode */
#define FEATURE_MC_ME_HAS_STOP0_MODE (1U)
/* @brief Mode entry has STANDBY mode */
#define FEATURE_MC_ME_HAS_STANDBY_MODE (0U)
/* @brief Mode entry allow to configured flash bit field*/
#define FEATURE_MC_ME_HAS_FLAON_CONFIG (1U)
/* @brief Mode entry has FLASH in power down mode */
#define FEATURE_MC_ME_HAS_FLAON_PD_MODE (0U)
/* @brief Mode entry has FLASH in low power mode */
#define FEATURE_MC_ME_HAS_FLAON_LP_MODE (0U)
/* @brief Enable or disable SDPLL clock */
#define FEATURE_HAS_SDPLL_CLK_CONFIG (0U)
/* @brief Number of cores. */
#define NUMBER_OF_CORES (2u)

/* WDOG module features */

/* @brief The key values to clear Soft Lock bit */
#define FEATURE_WDOG_UNLOCK_VALUE1  (0xC520U)
#define FEATURE_WDOG_UNLOCK_VALUE2  (0xD928U)

/* PIT module features */

/*! @brief PIT module has RTI channel */
#define FEATURE_PIT_HAS_RTI_CHANNEL (1U)
 /*! @brief Number of interrupt vector for channels of the PIT module */
#define FEATURE_PIT_HAS_NUM_IRQS_CHANS (PIT_IRQS_CH_COUNT)
/*! @brief PIT module has a peculiar instance */
#define FEATURE_PIT_HAS_PECULIAR_INSTANCE   (1U)
/*! @brief The default value of MDIS bit */
#define PIT_MCR_MDIS_DEFAULT                (1U)
/*! @brief PIT instance has not lifetime timer */
#define PIT_INSTANCE_HAS_NOT_LIFETIME_TIMER      (1U)

#if PIT_INSTANCE_HAS_NOT_LIFETIME_TIMER
/*! @brief PIT instance base has not lifetime timer */
#define PIT_INSTANCE_BASE_HAS_NOT_LIFETIME_TIMER   (PIT_0)
#endif

#if FEATURE_PIT_HAS_PECULIAR_INSTANCE
/*! @brief The peculiar instance */
#define PIT_PECULIAR_INSTANCE    (1U)
/*! @brief The number channel of the peculiar instance */
#define PIT_CHAN_NUM_OF_PECULIAR_INSTANCE   (2U)
#endif

#if FEATURE_PIT_HAS_RTI_CHANNEL
/*! @brief The default value of MDIS-RTI bit */
#define PIT_MCR_MDIS_RTI_DEFAULT            (1U)
/*! @brief Clock names for RTI. */
#define RTI_CLOCK_NAMES    {IRCOSC_CLK}
#endif
/*! @brief Clock names for PIT. */
#define PIT_CLOCK_NAMES    {PITRTI0_CLK, PITRTI1_CLK}

/* SWI2C features */
#define SWI2C_INSTANCE_COUNT                   0xFFU

/* STM module features */

/*! @brief STM module CR register has CSL bit-field */
#define FEATURE_STM_HAS_CLOCK_SELECTION (0U)
/*! @brief Number of interrupt vector for channels of the STM module */
#define FEATURE_STM_HAS_NUM_IRQS_CHANS  (4U)

#define STM0_Ch0_IRQHandler   STM0_CIR0_IRQHandler
#define STM0_Ch1_IRQHandler   STM0_CIR1_IRQHandler
#define STM0_Ch2_IRQHandler   STM0_CIR2_IRQHandler
#define STM0_Ch3_IRQHandler   STM0_CIR3_IRQHandler

#define STM1_Ch0_IRQHandler   STM1_CIR0_IRQHandler
#define STM1_Ch1_IRQHandler   STM1_CIR1_IRQHandler
#define STM1_Ch2_IRQHandler   STM1_CIR2_IRQHandler
#define STM1_Ch3_IRQHandler   STM1_CIR3_IRQHandler

/* SWT module features */

/* @brief Support service mode watchpoint input */
#define FEATURE_SWT_SUPPORT_WATCHPOINT      (0U)
/* @brief Support for clock selection */
#define FEATURE_SWT_HAS_CLOCK_SELECT        (0U)
/* @brief Support for running in stop mode */
#define FEATURE_SWT_HAS_STOP_MODE           (1U)
/* @brief The key values to clear Soft Lock bit */
#define FEATURE_SWT_UNLOCK_VALUE1           (0xC520U)
#define FEATURE_SWT_UNLOCK_VALUE2           (0xD928U)
/* @brief The key values used for resetting the SWT counter in Fixed Service Sequence Mode */
#define FEATURE_SWT_FIXED_SERVICE_VALUE1    (0xA602U)
#define FEATURE_SWT_FIXED_SERVICE_VALUE2    (0xB480U)
/* @brief The reset value of the control register */
#define FEATURE_SWT_CR_RESET_VALUE          (0xFF00010AU)
/* @brief The reset value of the timeout register */
#define FEATURE_SWT_TO_RESET_VALUE          (0x3FDE0U)
/* @brief The reset value of the window register */
#define FEATURE_SWT_WN_RESET_VALUE          (0x0U)
/* @brief The reset value of the service key register */
#define FEATURE_SWT_SK_RESET_VALUE          (0x0U)
/* @brief The minimum timeout value */
#define FEATURE_SWT_TO_MINIMUM_VALUE        (0x100U)
/* @brief Sets the master access protection field */
#define FEATURE_SWT_MAP_MASK                (0xFF000000U)
#define FEATURE_SWT_MAP_SHIFT               (24U)
#define FEATURE_SWT_MAP(x)                  (((uint32_t)(((uint32_t)(x)) << FEATURE_SWT_MAP_SHIFT)) & FEATURE_SWT_MAP_MASK)

/* WKPU module features */

/* @brief WKPU core source. */
typedef enum
{
    WKPU_CORE0    = 0U    /*!< Core 0 */
} wkpu_core_t;

/* @brief The WKPU core array */
#define FEATURE_WKPU_CORE_ARRAY     \
{                                   \
    WKPU_CORE0    /*!< Core 0 */    \
}

/*! @brief The number core support for WKPU module */
#define FEATURE_WKPU_NMI_NUM_CORES                      (1U)
/*! @brief The distance between cores */
#define FEATURE_WKPU_CORE_OFFSET_SIZE                   (8U)
/*! @brief WKPU support non-maskable interrupt */
#define FEATURE_WKPU_SUPPORT_NON_MASK_INT               (1U)
/*! @brief WKPU support critical interrupt */
#define FEATURE_WKPU_SUPPORT_CRITICAL_INT               (2U)
/*! @brief WKPU support machine check request interrupt */
#define FEATURE_WKPU_SUPPORT_MACHINE_CHK_REQ            (3U)
/*! @brief WKPU  isn't generated NMI, critical interrupt, or machine check request  */
#define FEATURE_WKPU_SUPPORT_NONE_REQUEST               (4U)

/* CRC module features */

/* @brief CRC module use for C55. */
#define FEATURE_CRC_DRIVER_HARD_POLYNOMIAL
/* @brief Default CRC read transpose */
#define FEATURE_CRC_DEFAULT_READ_TRANSPOSE      false
/* @brief Default CRC write transpose */
#define FEATURE_CRC_DEFAULT_WRITE_TRANSPOSE     CRC_TRANSPOSE_BITS
/* @brief Default CRC bit mode polynomial */
#define FEATURE_CRC_DEFAULT_POLYNOMIAL          CRC_BITS_16_CCITT
/* @brief Default seed value is 0xFFFFU */
#define FEATURE_CRC_DEFAULT_SEED                (0xFFFFU)
/* @brief CRC-8-H2F Autosar polynomial support */
#define FEATURE_CRC_BITS_8_H2F                  (1U)

/* SWI2C features */
#define SWI2C_INSTANCE_COUNT                   0xFFU

/* SMPU module features */

/* @brief Specifies the SMPU hardware and definition revision level */
#define FEATURE_SMPU_HARDWARE_REVISION_LEVEL (1U)
/* @brief Specifies the SMPU has process identifier feature */
#define FEATURE_SMPU_HAS_PROCESS_IDENTIFIER  (0U)
/* @brief Specifies the SMPU has owner lock feature */
#define FEATURE_SMPU_HAS_OWNER_LOCK          (0U)
/* @brief Specifies the SMPU has specific access feature */
#define FEATURE_SMPU_HAS_SPECIFIC_ACCESS_RIGHT_COUNT (0U)
/* @brief Specifies the SMPU has specific instance feature */
#define FEATURE_SMPU_HAS_SPECIFIC_INSTANCE   (1U)
/* @brief Specifies the SMPU has specific region descriptor feature */
#define FEATURE_SMPU_HAS_SPECIFIC_RGD_COUNT  (8U)
/* @brief Specifies the end address reset value */
#define FEATURE_SMPU_END_ADDRESS_RESET_VALUE (0U)
/* @brief Specifies the highest bus master */
#define FEATURE_SMPU_MAX_MASTER_NUMBER       (15U)
/* @brief Specifies total number of bus masters */
#define FEATURE_SMPU_MASTER_COUNT            (7U)
/* @brief The SMPU Logical Bus Master Number for Core 0 master */
#define FEATURE_SMPU_MASTER_CORE0            (0U)
/* @brief The SMPU Logical Bus Master Number for Core 1 master */
#define FEATURE_SMPU_MASTER_CORE1            (1U)
/* @brief The SMPU Logical Bus Master Number for Enhanced Direct Memory Access master */
#define FEATURE_SMPU_MASTER_DMA0             (3U)
/* @brief The SMPU Logical Bus Master Number for Ethernet master */
#define FEATURE_SMPU_MASTER_ENET             (4U)
/* @brief The SMPU Logical Bus Master Number for Serial Interprocessor Interface master */
#define FEATURE_SMPU_MASTER_ZIPWIRE          (7U)
/* @brief The SMPU Logical Bus Master Number for Core 0 Debug master */
#define FEATURE_SMPU_MASTER_DEBUG0           (8U)
/* @brief The SMPU Logical Bus Master Number for Core 1 Debug master */
#define FEATURE_SMPU_MASTER_DEBUG1           (9U)
/* @brief The SMPU Logical Bus Masters */
#define FEATURE_SMPU_MASTER                     \
{                                               \
FEATURE_SMPU_MASTER_CORE0,   /* CORE 0 */       \
FEATURE_SMPU_MASTER_CORE1,   /* CORE 1 */       \
FEATURE_SMPU_MASTER_DMA0,    /* DMA */          \
FEATURE_SMPU_MASTER_ENET,    /* ENET */         \
FEATURE_SMPU_MASTER_ZIPWIRE, /* ZIPWIRE */      \
FEATURE_SMPU_MASTER_DEBUG0,  /* CORE 0 DEBUG */ \
FEATURE_SMPU_MASTER_DEBUG1,  /* CORE 1 DEBUG */ \
}

/* SEMA42 module features */

/* @brief The SEMA42 Logical Bus Master Number for Core Z4a master */
#define FEATURE_SEMA42_MASTER_CORE_Z4A         (0U)
/* @brief The SEMA42 Logical Bus Master Number for Core Z4b master */
#define FEATURE_SEMA42_MASTER_CORE_Z4B         (1U)

/* @brief The SEMA42 Logical Bus Masters */
#define FEATURE_SEMA42_MASTER                      \
{                                                  \
FEATURE_SEMA42_MASTER_CORE_Z4A,  /* CORE Z4A */    \
FEATURE_SEMA42_MASTER_CORE_Z4B,  /* CORE Z4B */    \
}

/* @brief Fast IRC trimmed clock frequency(16MHz). */
#define FEATURE_IRCOSC0_FREQ (16000000U)

/* @brief Fast XOSC clock frequency(40MHz). */
#define FEATURE_XOSC0_FREQ  (40000000U)

/* @brief Define identifiers of auxiliary clock selectors */
#define NO_AC     0U
#define AC0__SC   1U
#define AC1__SC   2U
#define AC2__SC   3U
#define AC3__SC   4U
#define AC4__SC   5U
#define AC5__SC   6U
#define AC6__SC   7U
#define AC7__SC   8U
#define AC8__SC   9U
#define AC9__SC   10U
#define AC10__SC  11U
#define AC11__SC  12U
#define AC12__SC  13U
#define AC13__SC  14U
#define AC14__SC  15U
#define AC15__SC  16U
#define AC0__DC0  17U
#define AC0__DC1  18U
#define AC0__DC2  19U
#define AC0__DC3  20U
#define AC0__DC4  21U
#define AC1__DC0  22U
#define AC1__DC1  23U
#define AC1__DC2  24U
#define AC1__DC3  25U
#define AC2__DC0  26U
#define AC2__DC1  27U
#define AC2__DC2  28U
#define AC2__DC3  29U
#define AC3__DC0  30U
#define AC3__DC1  40U
#define AC3__DC2  41U
#define AC3__DC3  42U
#define AC4__DC0  43U
#define AC4__DC1  44U
#define AC4__DC2  45U
#define AC4__DC3  46U
#define AC5__DC0  47U
#define AC5__DC1  48U
#define AC5__DC2  49U
#define AC5__DC3  50U
#define AC6__DC0  51U
#define AC6__DC1  52U
#define AC6__DC2  53U
#define AC6__DC3  54U
#define AC7__DC0  55U
#define AC7__DC1  56U
#define AC7__DC2  57U
#define AC7__DC3  58U
#define AC8__DC0  59U
#define AC8__DC1  60U
#define AC8__DC2  61U
#define AC8__DC3  62U
#define AC9__DC0  63U
#define AC9__DC1  64U
#define AC9__DC2  65U
#define AC9__DC3  66U
#define AC10__DC0 67U
#define AC10__DC1 68U
#define AC10__DC2 69U
#define AC10__DC3 70U
#define AC11__DC0 71U
#define AC11__DC1 72U
#define AC11__DC2 73U
#define AC11__DC3 74U
#define AC12__DC0 75U
#define AC12__DC1 76U
#define AC12__DC2 77U
#define AC12__DC3 78U
#define AC13__DC0 79U
#define AC13__DC1 80U
#define AC13__DC2 81U
#define AC13__DC3 82U
#define AC14__DC0 83U
#define AC14__DC1 84U
#define AC14__DC2 85U
#define AC14__DC3 86U
#define AC15__DC0 87U
#define AC15__DC1 88U
#define AC15__DC2 89U
#define AC15__DC3 90U

#define SYSTEM_CLOCK_DIVIDER0_MASK (1U<<0U)
#define SYSTEM_CLOCK_DIVIDER1_MASK (1U<<1U)
#define SYSTEM_CLOCK_DIVIDER2_MASK (1U<<2U)
#define SYSTEM_CLOCK_DIVIDER3_MASK (1U<<3U)
#define SYSTEM_CLOCK_DIVIDER4_MASK (1U<<4U)
#define SYSTEM_CLOCK_DIVIDER5_MASK (1U<<5U)
#define SYSTEM_CLOCK_DIVIDER6_MASK (1U<<6U)

/* @brief List of the configurable system clock dividers. */
#define SYSTEM_CLOCK_DIVIDERS  (SYSTEM_CLOCK_DIVIDER0_MASK | SYSTEM_CLOCK_DIVIDER1_MASK | SYSTEM_CLOCK_DIVIDER2_MASK)

/* @brief PLL input reference. */
#define FEATURE_PLL0_INPUT_REFERENCE  AC3__SC
#define FEATURE_PLL1_INPUT_REFERENCE  AC4__SC

/* @brief PLL reduced frequency divider version. */
#define FEATURE_PLL_REDUCED_FREQ_DIV_VERSION (0U)

/* @brief FLEXCAN0 configurable interface clock. */
#define FEATURE_FLEXCAN0_CLK_CONFIGURABLE_INTERFACE_CLOCK (0U)

/* @brief PBRIDGEx system clock divider index. */
#define FEATURE_PBRIDGEx_CLK_SYSTEM_CLOCK_DIVIDER_INDEX (2U)

/* @brief PLL0_PHI1 reference entry value. */
#define FEATURE_PLL0_PHI1_REFERENCE_ENTRY_VALUE (3U)

/* @brief Synchronous mode for LIN is set using DCF clients */
#define FEATURE_LIN_SYNCHRONOUS_MODE		(0U)

/* @brief CMU reference. */
#define FEATURE_HAS_RCDIV1				  (0U)
#define FIRST_CMU						  CMU_ADCSD


#define FEATURE_HAS_SIRC_CLK             (0U)
#define FEATURE_HAS_FIRC_CLK             (0U)
#define FEATURE_HAS_IRCOSC_CLK           (1U)
#define FEATURE_HAS_SXOSC_CLK            (0U)
#define FEATURE_HAS_FXOSC_CLK            (0U)
#define FEATURE_HAS_XOSC_CLK             (1U)
#define FEATURE_HAS_PLL_PHI0_CLK         (0U)
#define FEATURE_HAS_PLL_PHI1_CLK         (0U)
#define FEATURE_HAS_PLL0_PHI0_CLK        (1U)
#define FEATURE_HAS_PLL0_PHI1_CLK        (1U)
#define FEATURE_HAS_PLL1_PHI0_CLK        (1U)
#define FEATURE_HAS_PLL1_PHI1_CLK        (0U)
#define FEATURE_HAS_SDPLL_CLK            (0U)
#define FEATURE_HAS_SCS_CLK              (1U)
#define FEATURE_HAS_SCS_CLKS             (0U)
#define FEATURE_HAS_S160_CLK             (0U)
#define FEATURE_HAS_S160_CLKS            (0U)
#define FEATURE_HAS_S80_CLK              (0U)
#define FEATURE_HAS_S80_CLKS             (0U)
#define FEATURE_HAS_S40_CLK              (0U)
#define FEATURE_HAS_S40_CLKS             (0U)
#define FEATURE_HAS_F40_CLK              (0U)
#define FEATURE_HAS_F40_CLKS             (0U)
#define FEATURE_HAS_F80_CLK              (0U)
#define FEATURE_HAS_F80_CLKS             (0U)
#define FEATURE_HAS_FS80_CLK             (0U)
#define FEATURE_HAS_FS80_CLKS            (0U)
#define FEATURE_HAS_F20_CLK              (0U)
#define FEATURE_HAS_F20_CLKS             (0U)
#define FEATURE_HAS_PBRIDGEx_CLK         (1U)
#define FEATURE_HAS_PBRIDGEx_CLKS        (1U)
#define FEATURE_HAS_SYS_CLK              (1U)
#define FEATURE_HAS_SYS_CLKS             (0U)
#define FEATURE_HAS_HALFSYS_CLK          (0U)
#define FEATURE_HAS_HALFSYS_CLKS         (0U)
#define FEATURE_HAS_MOTC_CLK             (0U)
#define FEATURE_HAS_MOTC_CLKS            (0U)
#define FEATURE_HAS_PER_CLK              (1U)
#define FEATURE_HAS_PER_CLKS             (0U)
#define FEATURE_HAS_FXBAR_CLK            (1U)
#define FEATURE_HAS_FXBAR_CLKS           (0U)
#define FEATURE_HAS_SXBAR_CLK            (1U)
#define FEATURE_HAS_SXBAR_CLKS           (1U)
#define FEATURE_HAS_DMA_CLK              (0U)
#define FEATURE_HAS_DMA_CLKS             (0U)
#define FEATURE_HAS_CORE0_CLK            (0U)
#define FEATURE_HAS_CORE0_CLKS           (0U)
#define FEATURE_HAS_CORE1_CLK            (0U)
#define FEATURE_HAS_CORE1_CLKS           (0U)
#define FEATURE_HAS_CORE2_CLK            (0U)
#define FEATURE_HAS_CORE2_CLKS           (0U)
#define FEATURE_HAS_ADC_CLKS             (1U)
#define FEATURE_HAS_ADCSD_CLKS           (1U)
#define FEATURE_HAS_DSPI_CLKS            (1U)
#define FEATURE_HAS_DSPIM_CLKS           (1U)
#define FEATURE_HAS_ENET_CLKS            (1U)
#define FEATURE_HAS_ENET_TIME_CLKS       (0U)
#define FEATURE_HAS_EMIOS_CLKS           (1U)
#define FEATURE_HAS_ETPU_CLKS            (1U)
#define FEATURE_HAS_FLEXCAN_CLKS         (1U)
#define FEATURE_HAS_FLEXRAY_CLKS         (0U)
#define FEATURE_HAS_LFAST_CLKS           (1U)
#define FEATURE_HAS_LIN_CLKS             (1U)
#define FEATURE_HAS_RTI_CLKS             (1U)
#define FEATURE_HAS_SDHC_CLKS            (0U)
#define FEATURE_HAS_SENT_CLKS            (1U)
#define FEATURE_HAS_SGEN_CLKS            (0U)
#define FEATURE_HAS_SPI_CLKS             (0U)
#define FEATURE_HAS_SPT_CLKS             (0U)
#define FEATURE_HAS_IRCOSC_CLKS          (1U)


#define FEATURE_PROTOCOL_CLOCK_FOR_ADC                 AC0__SC
#define FEATURE_PROTOCOL_CLOCK_FOR_ADCSD               AC0__SC
#define FEATURE_PROTOCOL_CLOCK_FOR_DSPI                AC0__SC
#define FEATURE_PROTOCOL_CLOCK_FOR_DSPIM               AC0__SC
#define FEATURE_PROTOCOL_CLOCK_FOR_ENET                AC10__SC
#define FEATURE_PROTOCOL_CLOCK_FOR_ENET_TIME           NO_AC
#define FEATURE_PROTOCOL_CLOCK_FOR_EMIOS               AC5__SC
#define FEATURE_PROTOCOL_CLOCK_FOR_ETPU                AC5__SC
#define FEATURE_PROTOCOL_CLOCK_FOR_FLEXCAN             AC8__SC
#define FEATURE_PROTOCOL_CLOCK_FOR_FLEXRAY             NO_AC
#define FEATURE_PROTOCOL_CLOCK_FOR_LFAST               AC1__SC
#define FEATURE_PROTOCOL_CLOCK_FOR_LIN                 AC0__SC
#define FEATURE_PROTOCOL_CLOCK_FOR_RTI                 AC9__SC
#define FEATURE_PROTOCOL_CLOCK_FOR_SDHC                NO_AC
#define FEATURE_PROTOCOL_CLOCK_FOR_SENT                AC2__SC
#define FEATURE_PROTOCOL_CLOCK_FOR_SGEN                NO_AC
#define FEATURE_PROTOCOL_CLOCK_FOR_SPI                 NO_AC
#define FEATURE_PROTOCOL_CLOCK_FOR_SPT                 NO_AC
#define FEATURE_INTERFACE_CLOCK_FOR_ADC                NO_AC
#define FEATURE_INTERFACE_CLOCK_FOR_ADCSD              NO_AC
#define FEATURE_INTERFACE_CLOCK_FOR_DSPI               NO_AC
#define FEATURE_INTERFACE_CLOCK_FOR_DSPIM              NO_AC
#define FEATURE_INTERFACE_CLOCK_FOR_ENET               NO_AC
#define FEATURE_INTERFACE_CLOCK_FOR_ENET_TIME          NO_AC
#define FEATURE_INTERFACE_CLOCK_FOR_EMIOS              NO_AC
#define FEATURE_INTERFACE_CLOCK_FOR_ETPU               NO_AC
#define FEATURE_INTERFACE_CLOCK_FOR_FLEXCAN            NO_AC
#define FEATURE_INTERFACE_CLOCK_FOR_FLEXRAY            NO_AC
#define FEATURE_INTERFACE_CLOCK_FOR_LFAST              NO_AC
#define FEATURE_INTERFACE_CLOCK_FOR_LIN                NO_AC
#define FEATURE_INTERFACE_CLOCK_FOR_RTI                NO_AC
#define FEATURE_INTERFACE_CLOCK_FOR_SDHC               NO_AC
#define FEATURE_INTERFACE_CLOCK_FOR_SENT               NO_AC
#define FEATURE_INTERFACE_CLOCK_FOR_SGEN               NO_AC
#define FEATURE_INTERFACE_CLOCK_FOR_SPI                NO_AC
#define FEATURE_INTERFACE_CLOCK_FOR_SPT                NO_AC
#define FEATURE_FRACTIONAL_DIVIDER_FOR_ADC             AC0__DC2
#define FEATURE_FRACTIONAL_DIVIDER_FOR_ADCSD           AC0__DC1
#define FEATURE_FRACTIONAL_DIVIDER_FOR_DSPI            AC0__DC4
#define FEATURE_FRACTIONAL_DIVIDER_FOR_DSPIM           AC0__DC3
#define FEATURE_FRACTIONAL_DIVIDER_FOR_ENET            AC10__DC0
#define FEATURE_FRACTIONAL_DIVIDER_FOR_ENET_TIME       NO_AC
#define FEATURE_FRACTIONAL_DIVIDER_FOR_EMIOS           AC5__DC1
#define FEATURE_FRACTIONAL_DIVIDER_FOR_ETPU            AC5__DC0
#define FEATURE_FRACTIONAL_DIVIDER_FOR_FLEXCAN         AC8__DC0
#define FEATURE_FRACTIONAL_DIVIDER_FOR_FLEXRAY         NO_AC
#define FEATURE_FRACTIONAL_DIVIDER_FOR_LFAST           AC1__DC0
#define FEATURE_FRACTIONAL_DIVIDER_FOR_LIN             AC0__DC4
#define FEATURE_FRACTIONAL_DIVIDER_FOR_RTI             AC9__DC0
#define FEATURE_FRACTIONAL_DIVIDER_FOR_SDHC            NO_AC
#define FEATURE_FRACTIONAL_DIVIDER_FOR_SENT            AC2__DC0
#define FEATURE_FRACTIONAL_DIVIDER_FOR_SGEN            NO_AC
#define FEATURE_FRACTIONAL_DIVIDER_FOR_SPI             NO_AC
#define FEATURE_FRACTIONAL_DIVIDER_FOR_SPT             NO_AC


/*! @brief Clock names. */
typedef enum {
    /* Clock sources */
    IRCOSC_CLK                     = 0u,      /*!< IRCOSC_CLK clock source                */
    XOSC_CLK                       = 1u,      /*!< XOSC_CLK clock source                  */
    PLL0_PHI0_CLK                  = 2u,      /*!< PLL0_PHI0_CLK clock source             */
    PLL0_PHI1_CLK                  = 3u,      /*!< PLL0_PHI1_CLK clock source             */
    PLL1_PHI0_CLK                  = 4u,      /*!< PLL1_PHI0_CLK clock source             */
    END_OF_CLK_SOURCES             = 20u,      /*!< End of clock sources                  */
    /* System and common clocks */
    SCS_CLK                        = 21u,      /*!< SCS_CLK common clock                  */
    PBRIDGEx_CLK                   = 22u,      /*!< PBRIDGEx_CLK common clock             */
    SYS_CLK                        = 23u,      /*!< SYS_CLK common clock                  */
    PER_CLK                        = 24u,      /*!< PER_CLK common clock                  */
    FXBAR_CLK                      = 25u,      /*!< FXBAR_CLK common clock                */
    SXBAR_CLK                      = 26u,      /*!< SXBAR_CLK common clock                */
    CORE_CLK                       = 27u,      /*!< CORE_CLK                              */
    END_OF_SYSTEM_CLKS             = 40u,      /*!< End of common and system clocks       */
    CRC0_CLK                       = 41u,      /*!< CRC0_CLK clock source                 */
    CRC1_CLK                       = 42u,      /*!< CRC1_CLK clock source                 */
    SIPI0_CLK                      = 43u,      /*!< SIPI0_CLK clock source                */
    WKPU0_CLK                      = 44u,      /*!< WKPU0_CLK clock source                */
    END_OF_PBRIDGEx_CLKS           = 45u,      /*!< End of PBRIDGEx_CLK clocks            */
    DMA0_CLK                       = 46u,      /*!< DMA0_CLK clock source                 */
    DMAMUX0_CLK                    = 47u,      /*!< DMAMUX0_CLK clock source              */
    DMAMUX1_CLK                    = 48u,      /*!< DMAMUX1_CLK clock source              */
    DMAMUX2_CLK                    = 49u,      /*!< DMAMUX2_CLK clock source              */
    DMAMUX3_CLK                    = 50u,      /*!< DMAMUX3_CLK clock source              */
    END_OF_SXBAR_CLKS              = 51u,      /*!< End of SXBAR_CLK clocks               */
    ADC0_CLK                       = 52u,      /*!< ADC0_CLK clock source                 */
    ADC1_CLK                       = 53u,      /*!< ADC1_CLK clock source                 */
    ADC2_CLK                       = 54u,      /*!< ADC2_CLK clock source                 */
    ADC3_CLK                       = 55u,      /*!< ADC3_CLK clock source                 */
    END_OF_ADC_CLKS                = 56u,      /*!< End of ADC_CLK clocks                 */
    ADCSD0_CLK                     = 57u,      /*!< ADCSD0_CLK clock source               */
    ADCSD1_CLK                     = 58u,      /*!< ADCSD1_CLK clock source               */
    ADCSD2_CLK                     = 59u,      /*!< ADCSD2_CLK clock source               */
    END_OF_ADCSD_CLKS              = 60u,      /*!< End of ADCSD_CLK clocks               */
    DSPI0_CLK                      = 61u,      /*!< DSPI0_CLK clock source                */
    DSPI1_CLK                      = 62u,      /*!< DSPI1_CLK clock source                */
    DSPI2_CLK                      = 63u,      /*!< DSPI2_CLK clock source                */
    DSPI3_CLK                      = 64u,      /*!< DSPI3_CLK clock source                */
    DSPI4_CLK                      = 65u,      /*!< DSPI4_CLK clock source                */
    END_OF_DSPI_CLKS               = 66u,      /*!< End of DSPI_CLK clocks                */
    DSPIM0_CLK                     = 67u,      /*!< DSPIM0_CLK clock source               */
    DSPIM1_CLK                     = 68u,      /*!< DSPIM1_CLK clock source               */
    END_OF_DSPIM_CLKS              = 69u,      /*!< End of DSPIM_CLK clocks               */
    ENET0_CLK                      = 70u,      /*!< ENET0_CLK clock source                */
    END_OF_ENET_CLKS               = 71u,      /*!< End of ENET_CLK clocks                */
    BCTU0_CLK                      = 72u,      /*!< BCTU0_CLK clock source                */
    eMIOS0_CLK                     = 73u,      /*!< eMIOS0_CLK clock source               */
    eMIOS1_CLK                     = 74u,      /*!< eMIOS1_CLK clock source               */
    END_OF_EMIOS_CLKS              = 75u,      /*!< End of EMIOS_CLK clocks               */
    ETPU0_CLK                      = 76u,      /*!< ETPU0_CLK clock source                */
    END_OF_ETPU_CLKS               = 77u,      /*!< End of ETPU_CLK clocks                */
    FLEXCAN0_CLK                   = 78u,      /*!< FLEXCAN0_CLK clock source             */
    FLEXCAN1_CLK                   = 79u,      /*!< FLEXCAN1_CLK clock source             */
    FLEXCAN2_CLK                   = 80u,      /*!< FLEXCAN2_CLK clock source             */
    FLEXCAN3_CLK                   = 81u,      /*!< FLEXCAN3_CLK clock source             */
    END_OF_FLEXCAN_CLKS            = 82u,      /*!< End of FLEXCAN_CLK clocks             */
    LFAST0_CLK                     = 83u,      /*!< LFAST0_CLK clock source               */
    END_OF_LFAST_CLKS              = 84u,      /*!< End of LFAST_CLK clocks               */
    LINM0_CLK                      = 85u,      /*!< LINM0_CLK clock source                */
    LINM1_CLK                      = 86u,      /*!< LINM1_CLK clock source                */
    LIN0_CLK                       = 87u,      /*!< LIN0_CLK clock source                 */
    LIN1_CLK                       = 88u,      /*!< LIN1_CLK clock source                 */
    LIN2_CLK                       = 89u,      /*!< LIN2_CLK clock source                 */
    LIN3_CLK                       = 90u,      /*!< LIN3_CLK clock source                 */
    END_OF_LIN_CLKS                = 91u,      /*!< End of LIN_CLK clocks                 */
    PITRTI0_CLK                    = 92u,      /*!< PITRTI0_CLK clock source              */
    PITRTI1_CLK                    = 93u,      /*!< PITRTI1_CLK clock source              */
    END_OF_RTI_CLKS                = 94u,      /*!< End of RTI_CLK clocks                 */
    SENT0_CLK                      = 95u,      /*!< SENT0_CLK clock source                */
    SENT1_CLK                      = 96u,      /*!< SENT1_CLK clock source                */
    END_OF_SENT_CLKS               = 97u,      /*!< End of SENT_CLK clocks                */
    SIUL0_CLK                      = 98u,      /*!< SIUL0_CLK clock source                */
    END_OF_IRCOSC_CLKS             = 99u,      /*!< End of IRCOSC_CLK clocks              */
    DECFILT0_CLK                   = 100u,      /*!< DECFILT0_CLK clock source             */
    DECFILT1_CLK                   = 101u,      /*!< DECFILT1_CLK clock source             */
    MCB0_CLK                       = 102u,      /*!< MCB0_CLK clock source                 */
    MEMU0_CLK                      = 103u,      /*!< MEMU0_CLK clock source                */
    END_OF_PERIPHERAL_CLKS         = 104u,      /*!< End of peripheral clocks              */
    CLOCK_NAME_COUNT,
} clock_names_t;

#define MC_ME_INVALID_INDEX  MC_ME_PCTLn_COUNT

  /*! @brief MC_ME clock name mappings
   *  Mappings between clock names and peripheral clock control indexes.
   *  If there is no peripheral clock control index for a clock name,
   *  then the corresponding value is MC_ME_INVALID_INDEX.
   */
#define MC_ME_CLOCK_NAME_MAPPINGS \
{                                                                              \
MC_ME_INVALID_INDEX,                /*!< IRCOSC clock                                       0  */ \
MC_ME_INVALID_INDEX,                /*!< XOSC clock                                         1  */ \
MC_ME_INVALID_INDEX,                /*!< PLL0_PHI0 clock                                    2  */ \
MC_ME_INVALID_INDEX,                /*!< PLL0_PHI1 clock                                    3  */ \
MC_ME_INVALID_INDEX,                /*!< PLL1_PHI0 clock                                    4  */ \
MC_ME_INVALID_INDEX,                /*!< No clock entry in clock_names_t                    5  */ \
MC_ME_INVALID_INDEX,                /*!< No clock entry in clock_names_t                    6  */ \
MC_ME_INVALID_INDEX,                /*!< No clock entry in clock_names_t                    7  */ \
MC_ME_INVALID_INDEX,                /*!< No clock entry in clock_names_t                    8  */ \
MC_ME_INVALID_INDEX,                /*!< No clock entry in clock_names_t                    9  */ \
MC_ME_INVALID_INDEX,                /*!< No clock entry in clock_names_t                    10  */ \
MC_ME_INVALID_INDEX,                /*!< No clock entry in clock_names_t                    11  */ \
MC_ME_INVALID_INDEX,                /*!< No clock entry in clock_names_t                    12  */ \
MC_ME_INVALID_INDEX,                /*!< No clock entry in clock_names_t                    13  */ \
MC_ME_INVALID_INDEX,                /*!< No clock entry in clock_names_t                    14  */ \
MC_ME_INVALID_INDEX,                /*!< No clock entry in clock_names_t                    15  */ \
MC_ME_INVALID_INDEX,                /*!< No clock entry in clock_names_t                    16  */ \
MC_ME_INVALID_INDEX,                /*!< No clock entry in clock_names_t                    17  */ \
MC_ME_INVALID_INDEX,                /*!< No clock entry in clock_names_t                    18  */ \
MC_ME_INVALID_INDEX,                /*!< No clock entry in clock_names_t                    19  */ \
MC_ME_INVALID_INDEX,                /*!< END_OF_CLK_SOURCES                                 20  */  \
MC_ME_INVALID_INDEX,                /*!< SCS_CLK clock                                      21  */ \
MC_ME_INVALID_INDEX,                /*!< PBRIDGEx_CLK clock                                 22  */ \
MC_ME_INVALID_INDEX,                /*!< SYS_CLK clock                                      23  */ \
MC_ME_INVALID_INDEX,                /*!< PER_CLK clock                                      24  */ \
MC_ME_INVALID_INDEX,                /*!< FXBAR_CLK clock                                    25  */ \
MC_ME_INVALID_INDEX,                /*!< SXBAR_CLK clock                                    26  */ \
MC_ME_INVALID_INDEX,                /*!< No clock entry in clock_names_t                    27  */ \
MC_ME_INVALID_INDEX,                /*!< No clock entry in clock_names_t                    28  */ \
MC_ME_INVALID_INDEX,                /*!< No clock entry in clock_names_t                    29  */ \
MC_ME_INVALID_INDEX,                /*!< No clock entry in clock_names_t                    30  */ \
MC_ME_INVALID_INDEX,                /*!< No clock entry in clock_names_t                    31  */ \
MC_ME_INVALID_INDEX,                /*!< No clock entry in clock_names_t                    32  */ \
MC_ME_INVALID_INDEX,                /*!< No clock entry in clock_names_t                    33  */ \
MC_ME_INVALID_INDEX,                /*!< No clock entry in clock_names_t                    34  */ \
MC_ME_INVALID_INDEX,                /*!< No clock entry in clock_names_t                    35  */ \
MC_ME_INVALID_INDEX,                /*!< No clock entry in clock_names_t                    36  */ \
MC_ME_INVALID_INDEX,                /*!< No clock entry in clock_names_t                    37  */ \
MC_ME_INVALID_INDEX,                /*!< No clock entry in clock_names_t                    38  */ \
MC_ME_INVALID_INDEX,                /*!< No clock entry in clock_names_t                    39  */ \
MC_ME_INVALID_INDEX,                /*!< END_OF_SYSTEM_CLOCKS                               40  */  \
MC_ME_PCTL_CRC_0_INDEX,             /*!< CRC0 clock source                                  41 */ \
MC_ME_PCTL_CRC_1_INDEX,             /*!< CRC1 clock source                                  42 */ \
MC_ME_PCTL_SIPI_0_INDEX,            /*!< SIPI0 clock source                                 43 */ \
MC_ME_INVALID_INDEX,                /*!< WKPU0 clock source                                 44 */ \
MC_ME_INVALID_INDEX,                /*!< End of PBRIDGEx clocks                             45 */ \
MC_ME_INVALID_INDEX,                /*!< DMA0 clock source                                  46 */ \
MC_ME_PCTL_DMA_CH_MUX_0_2_INDEX,    /*!< DMAMUX0 clock source                               47 */ \
MC_ME_PCTL_DMA_CH_MUX_0_2_INDEX,    /*!< DMAMUX1 clock source                               48 */ \
MC_ME_PCTL_DMA_CH_MUX_0_2_INDEX,    /*!< DMAMUX2 clock source                               49 */ \
MC_ME_PCTL_DMA_CH_MUX_3_INDEX,      /*!< DMAMUX3 clock source                               50 */ \
MC_ME_INVALID_INDEX,                /*!< End of SXBAR clocks                                51 */ \
MC_ME_PCTL_ADCSAR_0_INDEX,          /*!< ADC0 clock source                                  52 */ \
MC_ME_PCTL_ADCSAR_1_INDEX,          /*!< ADC1 clock source                                  53 */ \
MC_ME_PCTL_ADCSAR_2_INDEX,          /*!< ADC2 clock source                                  54 */ \
MC_ME_PCTL_ADCSAR_3_INDEX,          /*!< ADC3 clock source                                  55 */ \
MC_ME_INVALID_INDEX,                /*!< End of ADC clocks                                  56 */ \
MC_ME_PCTL_ADCSD_0_INDEX,           /*!< ADCSD0 clock source                                57 */ \
MC_ME_PCTL_ADCSD_1_INDEX,           /*!< ADCSD1 clock source                                58 */ \
MC_ME_PCTL_ADCSD_2_INDEX,           /*!< ADCSD2 clock source                                59 */ \
MC_ME_INVALID_INDEX,                /*!< End of ADCSD clocks                                60 */ \
MC_ME_PCTL_DSPI_0_INDEX,            /*!< DSPI0 clock source                                 61 */ \
MC_ME_PCTL_DSPI_1_INDEX,            /*!< DSPI1 clock source                                 62 */ \
MC_ME_PCTL_DSPI_2_INDEX,            /*!< DSPI2 clock source                                 63 */ \
MC_ME_PCTL_DSPI_3_INDEX,            /*!< DSPI3 clock source                                 64 */ \
MC_ME_PCTL_DSPI_4_INDEX,            /*!< DSPI4 clock source                                 65 */ \
MC_ME_INVALID_INDEX,                /*!< End of DSPI clocks                                 66 */ \
MC_ME_PCTL_DSPI_M0_INDEX,           /*!< DSPIM0 clock source                                67 */ \
MC_ME_PCTL_DSPI_M1_INDEX,           /*!< DSPIM1 clock source                                68 */ \
MC_ME_INVALID_INDEX,                /*!< End of DSPIM clocks                                69 */ \
MC_ME_INVALID_INDEX,                /*!< ENET0 clock source                                 70 */ \
MC_ME_INVALID_INDEX,                /*!< End of ENET clocks                                 71 */ \
MC_ME_PCTL_BCTU_INDEX,              /*!< BCTU0 clock source                                 72 */ \
MC_ME_PCTL_EMIOS_INDEX,             /*!< eMIOS0 clock source                                73 */ \
MC_ME_PCTL_EMIOS_1_INDEX,           /*!< eMIOS1 clock source                                74 */ \
MC_ME_INVALID_INDEX,                /*!< End of EMIOS clocks                                75 */ \
MC_ME_PCTL_ETPU_INDEX,              /*!< ETPU0 clock source                                 76 */ \
MC_ME_INVALID_INDEX,                /*!< End of ETPU clocks                                 77 */ \
MC_ME_PCTL_FLEXCAN_0_INDEX,         /*!< FLEXCAN0 clock source                              78 */ \
MC_ME_PCTL_FLEXCAN_1_INDEX,         /*!< FLEXCAN1 clock source                              79 */ \
MC_ME_PCTL_FLEXCAN_2_INDEX,         /*!< FLEXCAN2 clock source                              80 */ \
MC_ME_PCTL_FLEXCAN_3_INDEX,         /*!< FLEXCAN3 clock source                              81 */ \
MC_ME_INVALID_INDEX,                /*!< End of FLEXCAN clocks                              82 */ \
MC_ME_PCTL_LFAST_0_INDEX,           /*!< LFAST0 clock source                                83 */ \
MC_ME_INVALID_INDEX,                /*!< End of LFAST clocks                                84 */ \
MC_ME_PCTL_LINFLEX_M0_INDEX,        /*!< LINM0 clock source                                 85 */ \
MC_ME_PCTL_LINFLEX_M1_INDEX,        /*!< LINM1 clock source                                 86 */ \
MC_ME_PCTL_LINFLEX_0_INDEX,         /*!< LIN0 clock source                                  87 */ \
MC_ME_PCTL_LINFLEX_1_INDEX,         /*!< LIN1 clock source                                  88 */ \
MC_ME_PCTL_LINFLEX_2_INDEX,         /*!< LIN2 clock source                                  89 */ \
MC_ME_PCTL_LINFLEX_3_INDEX,         /*!< LIN3 clock source                                  90 */ \
MC_ME_INVALID_INDEX,                /*!< End of LIN clocks                                  91 */ \
MC_ME_PCTL_PIT_RTI_0_INDEX,         /*!< PITRTI0 clock source                               92 */ \
MC_ME_PCTL_PIT_RTI_1_INDEX,         /*!< PITRTI1 clock source                               93 */ \
MC_ME_INVALID_INDEX,                /*!< End of RTI clocks                                  94 */ \
MC_ME_PCTL_SENT_0_INDEX,            /*!< SENT0 clock source                                 95 */ \
MC_ME_PCTL_SENT_1_INDEX,            /*!< SENT1 clock source                                 96 */ \
MC_ME_INVALID_INDEX,                /*!< End of SENT clocks                                 97 */ \
MC_ME_INVALID_INDEX,                /*!< SIUL0 clock source                                 98 */ \
MC_ME_INVALID_INDEX,                /*!< End of IRCOSC clocks                               99 */ \
MC_ME_PCTL_DECFILT_0_INDEX,         /*!< DECFILT0 clock source                              100 */ \
MC_ME_PCTL_DECFILT_1_INDEX,         /*!< DECFILT1 clock source                              101 */ \
MC_ME_INVALID_INDEX,                /*!< MCB0 clock source                                  102 */ \
MC_ME_INVALID_INDEX,                /*!< MEMU0 clock source                                 103 */ \
MC_ME_INVALID_INDEX                /*!< END_OF_PERIPHERAL_CLK_SOURCES                       104 */ \
}


  /*! @brief interface clocks
   *  Mappings between clock names and interface clocks.
   *  If no interface clock exists for a given clock name,
   *  then the corresponding value is CLOCK_NAME_COUNT.
   */
#define INTERFACE_CLOCKS \
{                                                                           \
CLOCK_NAME_COUNT,                /*!< IRCOSC clock                    0  */ \
CLOCK_NAME_COUNT,                /*!< XOSC clock                      1  */ \
CLOCK_NAME_COUNT,                /*!< PLL0_PHI0 clock                 2  */ \
CLOCK_NAME_COUNT,                /*!< PLL0_PHI1 clock                 3  */ \
CLOCK_NAME_COUNT,                /*!< PLL1_PHI0 clock                 4  */ \
CLOCK_NAME_COUNT,                /*!< No clock entry in clock_names_t 5  */ \
CLOCK_NAME_COUNT,                /*!< No clock entry in clock_names_t 6  */ \
CLOCK_NAME_COUNT,                /*!< No clock entry in clock_names_t 7  */ \
CLOCK_NAME_COUNT,                /*!< No clock entry in clock_names_t 8  */ \
CLOCK_NAME_COUNT,                /*!< No clock entry in clock_names_t 9  */ \
CLOCK_NAME_COUNT,                /*!< No clock entry in clock_names_t 10  */ \
CLOCK_NAME_COUNT,                /*!< No clock entry in clock_names_t 11  */ \
CLOCK_NAME_COUNT,                /*!< No clock entry in clock_names_t 12  */ \
CLOCK_NAME_COUNT,                /*!< No clock entry in clock_names_t 13  */ \
CLOCK_NAME_COUNT,                /*!< No clock entry in clock_names_t 14  */ \
CLOCK_NAME_COUNT,                /*!< No clock entry in clock_names_t 15  */ \
CLOCK_NAME_COUNT,                /*!< No clock entry in clock_names_t 16  */ \
CLOCK_NAME_COUNT,                /*!< No clock entry in clock_names_t 17  */ \
CLOCK_NAME_COUNT,                /*!< No clock entry in clock_names_t 18  */ \
CLOCK_NAME_COUNT,                /*!< No clock entry in clock_names_t 19  */ \
CLOCK_NAME_COUNT,                /*!< END_OF_CLK_SOURCES              20  */ \
CLOCK_NAME_COUNT,                /*!< SCS_CLK clock                   21  */ \
CLOCK_NAME_COUNT,                /*!< PBRIDGEx_CLK clock              22  */ \
CLOCK_NAME_COUNT,                /*!< SYS_CLK clock                   23  */ \
CLOCK_NAME_COUNT,                /*!< PER_CLK clock                   24  */ \
CLOCK_NAME_COUNT,                /*!< FXBAR_CLK clock                 25  */ \
CLOCK_NAME_COUNT,                /*!< SXBAR_CLK clock                 26  */ \
CLOCK_NAME_COUNT,                /*!< No clock entry in clock_names_t 27  */ \
CLOCK_NAME_COUNT,                /*!< No clock entry in clock_names_t 28  */ \
CLOCK_NAME_COUNT,                /*!< No clock entry in clock_names_t 29  */ \
CLOCK_NAME_COUNT,                /*!< No clock entry in clock_names_t 30  */ \
CLOCK_NAME_COUNT,                /*!< No clock entry in clock_names_t 31  */ \
CLOCK_NAME_COUNT,                /*!< No clock entry in clock_names_t 32  */ \
CLOCK_NAME_COUNT,                /*!< No clock entry in clock_names_t 33  */ \
CLOCK_NAME_COUNT,                /*!< No clock entry in clock_names_t 34  */ \
CLOCK_NAME_COUNT,                /*!< No clock entry in clock_names_t 35  */ \
CLOCK_NAME_COUNT,                /*!< No clock entry in clock_names_t 36  */ \
CLOCK_NAME_COUNT,                /*!< No clock entry in clock_names_t 37  */ \
CLOCK_NAME_COUNT,                /*!< No clock entry in clock_names_t 38  */ \
CLOCK_NAME_COUNT,                /*!< No clock entry in clock_names_t 39  */ \
CLOCK_NAME_COUNT,                /*!< END_OF_SYSTEM_CLOCKS            40  */ \
PBRIDGEx_CLK,                    /*!< CRC0 clock                      41 */ \
PBRIDGEx_CLK,                    /*!< CRC1 clock                      42 */ \
PBRIDGEx_CLK,                    /*!< SIPI0 clock                     43 */ \
PBRIDGEx_CLK,                    /*!< WKPU0 clock                     44 */ \
CLOCK_NAME_COUNT,                /*!< End of PBRIDGEx clocks          45 */ \
SXBAR_CLK,                       /*!< DMA0 clock                      46 */ \
SXBAR_CLK,                       /*!< DMAMUX0 clock                   47 */ \
SXBAR_CLK,                       /*!< DMAMUX1 clock                   48 */ \
SXBAR_CLK,                       /*!< DMAMUX2 clock                   49 */ \
SXBAR_CLK,                       /*!< DMAMUX3 clock                   50 */ \
CLOCK_NAME_COUNT,                /*!< End of SXBAR clocks             51 */ \
PBRIDGEx_CLK,                    /*!< ADC0 clock                      52 */ \
PBRIDGEx_CLK,                    /*!< ADC1 clock                      53 */ \
PBRIDGEx_CLK,                    /*!< ADC2 clock                      54 */ \
PBRIDGEx_CLK,                    /*!< ADC3 clock                      55 */ \
CLOCK_NAME_COUNT,                /*!< End of ADC clocks               56 */ \
PBRIDGEx_CLK,                    /*!< ADCSD0 clock                    57 */ \
PBRIDGEx_CLK,                    /*!< ADCSD1 clock                    58 */ \
PBRIDGEx_CLK,                    /*!< ADCSD2 clock                    59 */ \
CLOCK_NAME_COUNT,                /*!< End of ADCSD clocks             60 */ \
PBRIDGEx_CLK,                    /*!< DSPI0 clock                     61 */ \
PBRIDGEx_CLK,                    /*!< DSPI1 clock                     62 */ \
PBRIDGEx_CLK,                    /*!< DSPI2 clock                     63 */ \
PBRIDGEx_CLK,                    /*!< DSPI3 clock                     64 */ \
PBRIDGEx_CLK,                    /*!< DSPI4 clock                     65 */ \
CLOCK_NAME_COUNT,                /*!< End of DSPI clocks              66 */ \
PBRIDGEx_CLK,                    /*!< DSPIM0 clock                    67 */ \
PBRIDGEx_CLK,                    /*!< DSPIM1 clock                    68 */ \
CLOCK_NAME_COUNT,                /*!< End of DSPIM clocks             69 */ \
PBRIDGEx_CLK,                    /*!< ENET0 clock                     70 */ \
CLOCK_NAME_COUNT,                /*!< End of ENET clocks              71 */ \
PBRIDGEx_CLK,                    /*!< BCTU0 clock                     72 */ \
PBRIDGEx_CLK,                    /*!< eMIOS0 clock                    73 */ \
PBRIDGEx_CLK,                    /*!< eMIOS1 clock                    74 */ \
CLOCK_NAME_COUNT,                /*!< End of EMIOS clocks             75 */ \
PBRIDGEx_CLK,                    /*!< ETPU0 clock                     76 */ \
CLOCK_NAME_COUNT,                /*!< End of ETPU clocks              77 */ \
PBRIDGEx_CLK,                    /*!< FLEXCAN0 clock                  78 */ \
PBRIDGEx_CLK,                    /*!< FLEXCAN1 clock                  79 */ \
PBRIDGEx_CLK,                    /*!< FLEXCAN2 clock                  80 */ \
PBRIDGEx_CLK,                    /*!< FLEXCAN3 clock                  81 */ \
CLOCK_NAME_COUNT,                /*!< End of FLEXCAN clocks           82 */ \
PBRIDGEx_CLK,                    /*!< LFAST0 clock                    83 */ \
CLOCK_NAME_COUNT,                /*!< End of LFAST clocks             84 */ \
PBRIDGEx_CLK,                    /*!< LINM0 clock                     85 */ \
PBRIDGEx_CLK,                    /*!< LINM1 clock                     86 */ \
PBRIDGEx_CLK,                    /*!< LIN0 clock                      87 */ \
PBRIDGEx_CLK,                    /*!< LIN1 clock                      88 */ \
PBRIDGEx_CLK,                    /*!< LIN2 clock                      89 */ \
PBRIDGEx_CLK,                    /*!< LIN3 clock                      90 */ \
CLOCK_NAME_COUNT,                /*!< End of LIN clocks               91 */ \
PER_CLK,                         /*!< PITRTI0 clock                   92 */ \
PER_CLK,                         /*!< PITRTI1 clock                   93 */ \
CLOCK_NAME_COUNT,                /*!< End of RTI clocks               94 */ \
PBRIDGEx_CLK,                    /*!< SENT0 clock                     95 */ \
PBRIDGEx_CLK,                    /*!< SENT1 clock                     96 */ \
CLOCK_NAME_COUNT,                /*!< End of SENT clocks              97 */ \
PBRIDGEx_CLK,                    /*!< SIUL0 clock                     98 */ \
CLOCK_NAME_COUNT,                /*!< End of IRCOSC clocks            99 */ \
PBRIDGEx_CLK,                    /*!< DECFILT0 clock                  100 */ \
PBRIDGEx_CLK,                    /*!< DECFILT1 clock                  101 */ \
PBRIDGEx_CLK,                    /*!< MCB0 clock                      102 */ \
PBRIDGEx_CLK,                    /*!< MEMU0 clock                     103 */ \
CLOCK_NAME_COUNT                /*!< END_OF_PERIPHERAL_CLK_SOURCES    104 */ \
}


/* Interrupt module features */

/* @brief Lowest interrupt request number. */
#define FEATURE_INTERRUPT_IRQ_MIN         (SS0_IRQn)
/* @brief Highest interrupt request number. */
#define FEATURE_INTERRUPT_IRQ_MAX         (DECFILTER1_ERR_IRQn)
/* @brief Highest interrupt priority number. */
#define FEATURE_INTERRUPT_PRIO_MAX  (63u)
/* @brief Highest software interrupt request number. */
#define FEATURE_INTERRUPT_SOFTWARE_IRQ_MAX  (SS31_IRQn)
/* @brief Has software interrupt. */
#define FEATURE_INTERRUPT_HAS_SOFTWARE_IRQ  (1u)
/* @brief Has pending interrupt state. */
#define FEATURE_INTERRUPT_HAS_PENDING_STATE (0u)
/* @brief Has active interrupt state. */
#define FEATURE_INTERRUPT_HAS_ACTIVE_STATE  (0u)
/* @brief Default interrupt priority for enable interrupts. */
#define FEATURE_INTERRUPT_DEFAULT_PRIO  (63u)
/* @brief Multicore support for interrupts */
#define FEATURE_INTERRUPT_MULTICORE_SUPPORT  (1u)
/* @brief Mask to enable interrupts for all cores */
#define FEATURE_INTERRUPT_ENABLE_ON_ALL_CORES_MASK  (0xC000u)
/* @brief Available cores for this device */
#define FEATURE_INTERRUPT_CORE_0_ENABLED (1u)
#define FEATURE_INTERRUPT_CORE_1_ENABLED (1u)
#define FEATURE_INTERRUPT_CORE_2_ENABLED (0u)
/* @brief Registers in which the start of interrupt vector table needs to be configured */
#define FEATURE_INTERRUPT_INT_VECTORS {&INTC->IACKR0, &INTC->IACKR1}

/* FLASH C55 module features */

/* @brief Type of flash module is C55FMC. */
#define FEATURE_FLS_C55_C55FMC (1U)
/* @brief Type of flash module is C55MP. */
#define FEATURE_FLS_C55_C55MP (0U)
/* @brief Type of flash module is C55FP. */
#define FEATURE_FLS_C55_C55FP (0U)
/* @brief Over-program protection enabled block. */
#define FEATURE_FLS_C55_HAS_OTP
/* @brief Has alternate interface. */
#define FEATURE_FLS_C55_HAS_ALTERNATE (0U)
/* @brief Has erase operation in the alternate interface. */
#define FEATURE_FLS_C55_HAS_ERASE_ALTERNATE (0U)
/* @brief The size of writes that are allowed. */
#define FLASH_C55_PROGRAMABLE_SIZE (0x80U)
/* @brief The address in the uTest space. */
#define FLASH_C55_INTERLOCK_WRITE_UTEST_ADDRESS (0x00400000U)
/* @brief The base address of the 16KB high block. */
#define FLASH_C55_16KB_HIGH_BASE_ADDRESS (0xFFFFFFFFU)
/* @brief The base address of the 32KB high block. */
#define FLASH_C55_32KB_HIGH_BASE_ADDRESS (0xFFFFFFFFU)
/* @brief The base address of the 64KB high block. */
#define FLASH_C55_64KB_HIGH_BASE_ADDRESS (0xFFFFFFFFU)
/* @brief The base address of the 16KB mid block. */
#define FLASH_C55_16KB_MID_BASE_ADDRESS (0x00800000U)
/* @brief The base address of the 32KB mid block. */
#define FLASH_C55_32KB_MID_BASE_ADDRESS (0xFFFFFFFFU)
/* @brief The base address of the 64KB mid block. */
#define FLASH_C55_64KB_MID_BASE_ADDRESS (0x00820000U)
/* @brief The base address of the 16KB low block. */
#define FLASH_C55_16KB_LOW_BASE_ADDRESS (0x00404000U)
/* @brief The base address of the 32KB low block. */
#define FLASH_C55_32KB_LOW_BASE_ADDRESS (0x00FB0000U)
/* @brief The base address of the 64KB low block. */
#define FLASH_C55_64KB_LOW_BASE_ADDRESS (0x00FC0000U)
/* @brief The base address of the 256KB block. */
#define FLASH_C55_256KB_BASE_ADDRESS (0x01000000U)
/* @brief The number of low block */
#define NUM_LOW_BLOCK                          12U
/* @brief The number of mid block */
#define NUM_MID_BLOCK                          10U
/* @brief The number of high block */
#define NUM_HIGH_BLOCK                         0U
/* @brief The number of 256k block */
#define NUM_256K_BLOCK_FIRST                    10U
#define NUM_256K_BLOCK_SECOND                   0U
#define NUM_256K_BLOCK                         (NUM_256K_BLOCK_FIRST + NUM_256K_BLOCK_SECOND)
#define NUM_BLOCK                              (NUM_LOW_BLOCK + NUM_MID_BLOCK + NUM_HIGH_BLOCK + NUM_256K_BLOCK)

#define LOW_BLOCK0_ADDR                        0x00404000U
#define LOW_BLOCK0_MASK                        (1UL << 0x0U)

#define LOW_BLOCK1_ADDR                        0x00FA0000U
#define LOW_BLOCK1_MASK                        (1UL << 0x1U)

#define LOW_BLOCK2_ADDR                        0x00FA4000U
#define LOW_BLOCK2_MASK                        (1UL << 0x2U)

#define LOW_BLOCK3_ADDR                        0x00FA8000U
#define LOW_BLOCK3_MASK                        (1UL << 0x3U)

#define LOW_BLOCK4_ADDR                        0x00FAC000U
#define LOW_BLOCK4_MASK                        (1UL << 0x4U)

#define LOW_BLOCK5_ADDR                        0x00F9C000U
#define LOW_BLOCK5_MASK                        (1UL << 0x5U)

#define LOW_BLOCK6_ADDR                        0x08FB0000U
#define LOW_BLOCK6_MASK                        (1UL << 0x6U)

#define LOW_BLOCK7_ADDR                        0x00FB8000U
#define LOW_BLOCK7_MASK                        (1UL << 0x7U)

#define LOW_BLOCK8_ADDR                        0x00FC0000U
#define LOW_BLOCK8_MASK                        (1UL << 0x8U)

#define LOW_BLOCK9_ADDR                        0x00FD0000U
#define LOW_BLOCK9_MASK                        (1UL << 0x9U)

#define LOW_BLOCK10_ADDR                        0x00FE0000U
#define LOW_BLOCK10_MASK                        (1UL << 0xAU)

#define LOW_BLOCK11_ADDR                        0x00FF0000U
#define LOW_BLOCK11_MASK                        (1UL << 0xBU)

/* Define Macros for mid blocks */
#define MID_BLOCK0_ADDR                        0x00800000U
#define MID_BLOCK0_MASK                        (1UL << 0x0U)

#define MID_BLOCK1_ADDR                        0x00804000U
#define MID_BLOCK1_MASK                        (1UL << 0x1U)

#define MID_BLOCK2_ADDR                        0x00808000U
#define MID_BLOCK2_MASK                        (1UL << 0x2U)

#define MID_BLOCK3_ADDR                        0x0080C000U
#define MID_BLOCK3_MASK                        (1UL << 0x3U)

#define MID_BLOCK4_ADDR                        0x00810000U
#define MID_BLOCK4_MASK                        (1UL << 0x4U)

#define MID_BLOCK5_ADDR                        0x00814000U
#define MID_BLOCK5_MASK                        (1UL << 0x5U)

#define MID_BLOCK6_ADDR                        0x00818000U
#define MID_BLOCK6_MASK                        (1UL << 0x6U)

#define MID_BLOCK7_ADDR                        0x0081C000U
#define MID_BLOCK7_MASK                        (1UL << 0x7U)

#define MID_BLOCK8_ADDR                        0x00820000U
#define MID_BLOCK8_MASK                        (1UL << 0x8U)

#define MID_BLOCK9_ADDR                        0x00830000U
#define MID_BLOCK9_MASK                        (1UL << 0x9U)



/* Define Macros for 256k blocks */
#define N256K_BLOCK0_ADDR                       0x01000000U
#define N256K_BLOCK0_MASK                       (1UL << 0x0U)

#define N256K_BLOCK1_ADDR                       0x01040000U
#define N256K_BLOCK1_MASK                       (1UL << 0x1U)

#define N256K_BLOCK2_ADDR                       0x01080000U
#define N256K_BLOCK2_MASK                       (1UL << 0x2U)

#define N256K_BLOCK3_ADDR                       0x010C0000U
#define N256K_BLOCK3_MASK                       (1UL << 0x3U)

#define N256K_BLOCK4_ADDR                       0x01100000U
#define N256K_BLOCK4_MASK                       (1UL << 0x4U)

#define N256K_BLOCK5_ADDR                       0x01140000U
#define N256K_BLOCK5_MASK                       (1UL << 0x5U)

#define N256K_BLOCK6_ADDR                       0x01180000U
#define N256K_BLOCK6_MASK                       (1UL << 0x6U)

#define N256K_BLOCK7_ADDR                       0x011C0000U
#define N256K_BLOCK7_MASK                       (1UL << 0x7U)

#define N256K_BLOCK8_ADDR                       0x01200000U
#define N256K_BLOCK8_MASK                       (1UL << 0x8U)

#define N256K_BLOCK9_ADDR                       0x01240000U
#define N256K_BLOCK9_MASK                       (1UL << 0x9U)

#define FLASH_BLOCK_ADDR_DEFINE \
{                               \
LOW_BLOCK0_ADDR, /* Low block 0 address */ \
LOW_BLOCK1_ADDR, /* Low block 1 address */ \
LOW_BLOCK2_ADDR, /* Low block 2 address */ \
LOW_BLOCK3_ADDR, /* Low block 3 address */ \
LOW_BLOCK4_ADDR, /* Low block 4 address */ \
LOW_BLOCK5_ADDR, /* Low block 5 address */ \
LOW_BLOCK6_ADDR, /* Low block 6 address */ \
LOW_BLOCK7_ADDR, /* Low block 7 address */ \
LOW_BLOCK8_ADDR, /* Low block 8 address */ \
LOW_BLOCK9_ADDR, /* Low block 9 address */ \
LOW_BLOCK10_ADDR, /* Low block 10 address */ \
LOW_BLOCK11_ADDR, /* Low block 11 address */ \
MID_BLOCK0_ADDR, /* Mid block 0 address */ \
MID_BLOCK1_ADDR, /* Mid block 1 address */ \
MID_BLOCK2_ADDR, /* Mid block 2 address */ \
MID_BLOCK3_ADDR, /* Mid block 3 address */ \
MID_BLOCK4_ADDR, /* Mid block 4 address */  \
MID_BLOCK5_ADDR, /* Mid block 5 address */  \
MID_BLOCK6_ADDR, /* Mid block 6 address */  \
MID_BLOCK7_ADDR, /* Mid block 7 address */  \
MID_BLOCK8_ADDR, /* Mid block 8 address */  \
MID_BLOCK9_ADDR, /* Mid block 8 address */  \
N256K_BLOCK0_ADDR, /* 256K block 0 address */  \
N256K_BLOCK1_ADDR, /* 256K block 1 address */  \
N256K_BLOCK2_ADDR, /* 256K block 2 address */  \
N256K_BLOCK3_ADDR, /* 256K block 3 address */  \
N256K_BLOCK4_ADDR, /* 256K block 4 address */  \
N256K_BLOCK5_ADDR, /* 256K block 5 address */  \
N256K_BLOCK6_ADDR, /* 256K block 6 address */  \
N256K_BLOCK7_ADDR, /* 256K block 7 address */  \
N256K_BLOCK8_ADDR, /* 256K block 8 address */  \
N256K_BLOCK9_ADDR /* 256K block 9 address */  \
}
#define FLASH_BLOCK_MASK_DEFINE \
{                              \
LOW_BLOCK0_MASK, /* Low block 0 mask */ \
LOW_BLOCK1_MASK, /* Low block 1 mask */ \
LOW_BLOCK2_MASK, /* Low block 2 mask */ \
LOW_BLOCK3_MASK, /* Low block 3 mask */ \
LOW_BLOCK4_MASK, /* Low block 4 mask */ \
LOW_BLOCK5_MASK, /* Low block 5 mask */ \
LOW_BLOCK6_MASK, /* Low block 6 mask */ \
LOW_BLOCK7_MASK, /* Low block 7 mask */ \
LOW_BLOCK8_MASK, /* Low block 8 mask */ \
LOW_BLOCK9_MASK, /* Low block 9 mask */ \
LOW_BLOCK10_MASK, /* Low block 10 mask */ \
LOW_BLOCK11_MASK, /* Low block 11 mask */ \
MID_BLOCK0_MASK, /* Mid block 0 mask */ \
MID_BLOCK1_MASK, /* Mid block 1 mask */ \
MID_BLOCK2_MASK, /* Mid block 2 mask */ \
MID_BLOCK3_MASK, /* Mid block 3 mask */ \
MID_BLOCK4_MASK, /* Mid block 4 mask */  \
MID_BLOCK5_MASK, /* Mid block 5 mask */  \
MID_BLOCK6_MASK, /* Mid block 6 mask */  \
MID_BLOCK7_MASK, /* Mid block 7 mask */  \
MID_BLOCK8_MASK, /* Mid block 8 mask */  \
MID_BLOCK9_MASK, /* Mid block 8 mask */  \
N256K_BLOCK0_MASK, /* 256K block 0 mask */  \
N256K_BLOCK1_MASK, /* 256K block 1 mask */  \
N256K_BLOCK2_MASK, /* 256K block 2 mask */  \
N256K_BLOCK3_MASK, /* 256K block 3 mask */  \
N256K_BLOCK4_MASK, /* 256K block 4 mask */  \
N256K_BLOCK5_MASK, /* 256K block 5 mask */  \
N256K_BLOCK6_MASK, /* 256K block 6 mask */  \
N256K_BLOCK7_MASK, /* 256K block 7 mask */  \
N256K_BLOCK8_MASK, /* 256K block 8 mask */  \
N256K_BLOCK9_MASK /* 256K block 9 mask */  \
}

/* EEE module features */

/*  @brief The block endurance for erasing successful */
#define BLOCK_MAX_ENDURANCE       (250000U)
/*  @brief The EEC error will occur in IVOR exception handler */
#define EEE_ERR_IVOR_EXCEPTION    (0)
/*  @brief The EEC error will set an ECC error in MCR register */
#define EEE_ERR_C55_MCR           (1)
/*  @brief Has 4 bytes ECC */
#define EEE_ECC4                  (0)
/*  @brief Has 8 bytes ECC */
#define EEE_ECC8                  (1)
/*  @brief Has 16 bytes ECC */
#define EEE_ECC16                 (0)
/*  @brief Has 32 bytes ECC */
#define EEE_ECC32                 (0)

/* OSIF module features */

#define FEATURE_OSIF_USE_SYSTICK                         (0)
#define FEATURE_OSIF_USE_PIT                             (1)
#define FEATURE_OSIF_FREERTOS_ISR_CONTEXT_METHOD         (2) /* PowerPC FreeRTOS v9.0.0 */
#define OSIF_PIT (PIT_0)
#define OSIF_PIT_CHAN_ID (7u)

/* FCCU module features */

#define FEATURE_FCCU_UNLOCK_OP1                (0x913756AFU)
#define FEATURE_FCCU_UNLOCK_OP2                (0x825A132BU)
#define FEATURE_FCCU_UNLOCK_OP31               (0x29AF8752U)

#define FEATURE_FCCU_NCF_KEY                   (0xAB3498FEU)

#define FEATURE_FCCU_IRQ_EN_MASK               (0x1U)

#define FEATURE_FCCU_TRANS_UNLOCK              (0xBCU)
#define FEATURE_FCCU_PERMNT_LOCK               (0xFFU)
#define FEATURE_FCCU_EINOUT_EOUTX_MASK         (FCCU_EINOUT_EOUT0_MASK|FCCU_EINOUT_EOUT1_MASK)

#define FEATURE_FCCU_MAX_FAULTS_NO             (uint8_t)(95U)
/* FCUU filter feature */
#define FEATURE_FCCU_FILTER_EN                 (1U)
/* FCCU open drain for the error indicating pin(s) */
#define FEATURE_FCCU_OPEN_DRAIN_EN             (0U)
/* FCCU mode controller status */
#define FEATURE_FCCU_CONTROL_MODE_EN           (1U)
/* FCCU redundancy control checker */
#define FEATURE_FCCU_RCC_EN                    (1U)
/* FCCU Fault-Output (EOUT) Prescaler */
#define FEATURE_FCCU_FOP_SUPPORT

/* MSCM module features */

/* @brief Has interrupt router control registers (IRSPRCn). */
#define FEATURE_MSCM_HAS_INTERRUPT_ROUTER                (0)
/* @brief Has directed CPU interrupt routerregisters (IRCPxxx). */
#define FEATURE_MSCM_HAS_CPU_INTERRUPT_ROUTER            (0)


/* EMIOS module features */

/* @brief EMIOS CNT register 24 bits. */
#define FEATURE_EMIOS_COUNTER_24_BITS
/* @brief eMIOS disable channel register. */
#define FEATURE_EMIOS_UC_DISABLE
/* @brief EMIOS STAC Client. */
#define FEATURE_EMIOS_STAC_CLIENT
/* @brief EMIOS support all modes for each channel. */
#define FEATURE_EMIOS_ALL_MODE_ON_CHANNEL
/* @brief EMIOS has channel pair in the IRQ interrupt. */
#define FEATURE_EMIOS_CHANNELS_PER_IRQ          (1U)
/* @brief EMIOS number channels maximum. */
#define FEATURE_EMIOS_CHANNEL_MAX_COUNT         (32U)
/* @brief EMIOS channels and counter buses select. */
#define FEATURE_EMIOS_BUS_A_SELECT              (1U)
#define FEATURE_EMIOS_BUS_B_SELECT              (0U)
#define FEATURE_EMIOS_BUS_C_SELECT              (1U)
#define FEATURE_EMIOS_BUS_D_SELECT              (1U)
#define FEATURE_EMIOS_BUS_E_SELECT              (0U)
#define FEATURE_EMIOS_BUS_F_SELECT              (0U)
/* @brief EMIOS channels and counter buses offset. */
#define FEATURE_EMIOS_BUS_B_SELECT_OFFSET(x)    (x)
#define FEATURE_EMIOS_BUS_C_SELECT_OFFSET(x)    (x - 8U)
#define FEATURE_EMIOS_BUS_D_SELECT_OFFSET(x)    (x - 8U)
#define FEATURE_EMIOS_BUS_E_SELECT_OFFSET(x)    (x)
/* @brief EMIOS number of channels available. */
#define FEATURE_EMIOS_CH_7_0_ENABLE             (0U)
#define FEATURE_EMIOS_CH_15_8_ENABLE            (1U)
#define FEATURE_EMIOS_CH_23_16_ENABLE           (1U)
#define FEATURE_EMIOS_CH_31_24_ENABLE           (0U)
#define FEATURE_EMIOS_CH_COUNT                  ((FEATURE_EMIOS_CH_7_0_ENABLE   * 8U) + \
                                                 (FEATURE_EMIOS_CH_15_8_ENABLE  * 8U) + \
                                                 (FEATURE_EMIOS_CH_23_16_ENABLE * 8U) + \
                                                 (FEATURE_EMIOS_CH_31_24_ENABLE * 8U))
/* @brief EMIOS channels offset. */
#define FEATURE_EMIOS_CH_7_0_SUB_OFFSET(x)      (x)
#define FEATURE_EMIOS_CH_15_8_SUB_OFFSET(x)     (x - 8U)
#define FEATURE_EMIOS_CH_23_16_SUB_OFFSET(x)    (x - 8U)
#define FEATURE_EMIOS_CH_31_24_SUB_OFFSET(x)    (x)

/* PWM_PAL module features */
#define FEATURE_PWMPAL_EMIOS_HAS_CHANNEL_MAPPING    (1)
#define PWMPAL_INDEX_2_HW_CHANNELS {8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23}

/* LINFlexD module features */

/* @brief LINFlexD separate interrupt lines for rx/tx/error. */
#define FEATURE_LINFLEXD_RX_TX_ERR_INT_LINES
/* @brief Clock names for LINFlexD */
#define LINFLEXD_CLOCK_NAMES {LIN0_CLK, LIN1_CLK, LIN2_CLK,  LIN3_CLK,  LINM0_CLK,  LINM1_CLK}
/* @brief Address of the least significant byte in a 32-bit register (depends on endianess) */
#define FEATURE_LINFLEXD_LSB_ADDR(reg)  ((uint32_t)(&(reg)) + 3U)
/* @brief Address of the least significant word in a 32-bit register (depends on endianess) */
#define FEATURE_LINFLEXD_LSW_ADDR(reg)  ((uint32_t)(&(reg)) + 2U)
/* @brief LINFlexD "instance-implemented filters" mapping */
#define FEATURE_LINFLEXD_INST_HAS_IFCR {true, false, false, false, false, false}
/* @brief LINFlexD DMA support */
#define FEATURE_LINFLEXD_HAS_DMA_ENABLED
/* @brief LINFlexD DMA enabled instances */
#define FEATURE_LINFLEXD_INST_HAS_DMA {true, true, true, true, true, true}
/* @brief LINFlexD timeout interrupt enable bit mask */
#define LINFlexD_LINIER_TOIE_MASK LINFlexD_LINIER_DBEIETOIE_MASK
#define FEATURE_LINFLEXD_HAS_DIFFERENT_MEM_MAP
/* @brief LINFlexD register layout with 0 filters implemented (instances 1-17) */
typedef struct {
  __IO uint32_t LINCR1;                            /**< LIN Control Register 1, offset: 0x0 */
  __IO uint32_t LINIER;                            /**< LIN Interrupt enable register, offset: 0x4 */
  __IO uint32_t LINSR;                             /**< LIN Status Register, offset: 0x8 */
  __IO uint32_t LINESR;                            /**< LIN Error Status Register, offset: 0xC */
  __IO uint32_t UARTCR;                            /**< UART Mode Control Register, offset: 0x10 */
  __IO uint32_t UARTSR;                            /**< UART Mode Status Register, offset: 0x14 */
  __IO uint32_t LINTCSR;                           /**< LIN Time-Out Control Status Register, offset: 0x18 */
  __IO uint32_t LINOCR;                            /**< LIN Output Compare Register, offset: 0x1C */
  __IO uint32_t LINTOCR;                           /**< LIN Time-Out Control Register, offset: 0x20 */
  __IO uint32_t LINFBRR;                           /**< LIN Fractional Baud Rate Register, offset: 0x24 */
  __IO uint32_t LINIBRR;                           /**< LIN Integer Baud Rate Register, offset: 0x28 */
  __IO uint32_t LINCFR;                            /**< LIN Checksum Field Register, offset: 0x2C */
  __IO uint32_t LINCR2;                            /**< LIN Control Register 2, offset: 0x30 */
  __IO uint32_t BIDR;                              /**< Buffer Identifier Register, offset: 0x34 */
  __IO uint32_t BDRL;                              /**< Buffer Data Register Least Significant, offset: 0x38 */
  __IO uint32_t BDRM;                              /**< Buffer Data Register Most Significant, offset: 0x3C */
  __IO uint32_t IFER;                              /**< Identifier Filter Enable Register, offset: 0x40 */
  __I  uint32_t IFMI;                              /**< Identifier Filter Match Index, offset: 0x44 */
  __IO uint32_t IFMR;                              /**< Identifier Filter Mode Register, offset: 0x48 */
  __IO uint32_t GCR;                               /**< Global Control Register, offset: 0x8C */
  __IO uint32_t UARTPTO;                           /**< UART Preset Timeout Register, offset: 0x90 */
  __I  uint32_t UARTCTO;                           /**< UART Current Timeout Register, offset: 0x94 */
  __IO uint32_t DMATXE;                            /**< DMA Tx Enable Register, offset: 0x98 */
  __IO uint32_t DMARXE;                            /**< DMA Rx Enable Register, offset: 0x9C */
}LINFlexD_0IFCR_Type;

/* EDMA module features */

/* @brief DMA hardware version 2. */
#define FEATURE_DMA_HWV2
/* @brief Number of DMA channels. */
#define FEATURE_DMA_CHANNELS (64U)
/* @brief Number of DMA virtual channels. */
#define FEATURE_DMA_VIRTUAL_CHANNELS (FEATURE_DMA_CHANNELS * DMA_INSTANCE_COUNT)
/* @brief Number of DMA interrupt lines. */
#define FEATURE_DMA_CHANNELS_INTERRUPT_LINES (64U)
/* @brief Number of DMA virtual interrupt lines. */
#define FEATURE_DMA_VIRTUAL_CHANNELS_INTERRUPT_LINES ((uint32_t)FEATURE_DMA_CHANNELS_INTERRUPT_LINES * (uint32_t)DMA_INSTANCE_COUNT)
/* @brief Number of DMA error interrupt lines. */
#define FEATURE_DMA_ERROR_INTERRUPT_LINES (1U)
/* @brief Number of DMA virtual error interrupt lines. */
#define FEATURE_DMA_VIRTUAL_ERROR_INTERRUPT_LINES ((uint32_t)FEATURE_DMA_ERROR_INTERRUPT_LINES * (uint32_t)DMA_INSTANCE_COUNT)
/* @brief DMA module has error interrupt. */
#define FEATURE_DMA_HAS_ERROR_IRQ
/* @brief DMA module separate interrupt lines for each channel */
#define FEATURE_DMA_SEPARATE_IRQ_LINES_PER_CHN
/* @brief Conversion from channel index to DCHPRI index. */
#define FEATURE_DMA_CHN_TO_DCHPRI_INDEX(x) (x)
/* @brief DMA channel groups count. */
#define FEATURE_DMA_CHANNEL_GROUP_COUNT (2U)
/* @brief Clock name for DMA */
#define FEATURE_DMA_CLOCK_NAMES {DMA0_CLK}
/* @brief Macros defined for compatibility with ARM platforms */
#define DMA_TCD_CITER_ELINKYES_CITER_LE_MASK   DMA_TCD_CITER_ELINKYES_CITER_MASK
#define DMA_TCD_CITER_ELINKYES_CITER_LE_SHIFT  DMA_TCD_CITER_ELINKYES_CITER_SHIFT
#define DMA_TCD_CITER_ELINKYES_CITER_LE_WIDTH  DMA_TCD_CITER_ELINKYES_CITER_WIDTH
#define DMA_TCD_CITER_ELINKYES_CITER_LE(x)     DMA_TCD_CITER_ELINKYES_CITER(x)
/* @brief DMA channel width based on number of TCDs: 2^N, N=4,5,... */
#define FEATURE_DMA_CH_WIDTH (6U)
/* @brief DMA channel to instance */
#define FEATURE_DMA_VCH_TO_INSTANCE(x) 	((x) >> (uint32_t)FEATURE_DMA_CH_WIDTH)
/* @brief DMA virtual channel to channel */
#define FEATURE_DMA_VCH_TO_CH(x)        ((x) & ((uint32_t)FEATURE_DMA_CHANNELS - 1U))
/* @brief DMA supports the following particular transfer size: */
#define FEATURE_DMA_TRANSFER_SIZE_16B
#define FEATURE_DMA_TRANSFER_SIZE_32B


/* DMAMUX module features */

/* @brief DMAMUX peripheral is available in silicon. */
#define FEATURE_DMAMUX_AVAILABLE
/* @brief Number of DMA channels. */
#define FEATURE_DMAMUX_CHANNELS (16U)
/* @brief Has the periodic trigger capability */
#define FEATURE_DMAMUX_HAS_TRIG (1)
/* @brief Conversion from request source to the actual DMAMUX channel */
#define FEATURE_DMAMUX_REQ_SRC_TO_CH(x) ((x) & DMAMUX_CHCFG_SOURCE_MASK)
/* @brief Mapping between request source and DMAMUX instance */
#define FEATURE_DMAMUX_REQ_SRC_TO_INSTANCE(x) (((uint8_t)x) >> (uint8_t)DMAMUX_CHCFG_SOURCE_WIDTH)
/* @brief Conversion from eDMA channel index to DMAMUX channel. */
#define FEATURE_DMAMUX_DMA_CH_TO_CH(x) ((x) & (FEATURE_DMAMUX_CHANNELS - 1U))
/* @brief Conversion from DMAMUX channel DMAMUX register index. */
#define FEATURE_DMAMUX_CHN_REG_INDEX(x) (x)
/* @brief Clock names for DMAMUX */
#define FEATURE_DMAMUX_CLOCK_NAMES {DMAMUX0_CLK, DMAMUX1_CLK, DMAMUX2_CLK, DMAMUX3_CLK}
/*!
 * @brief Structure for the DMA hardware request
 *
 * Defines the structure for the DMA hardware request collections. The user can configure the
 * hardware request into DMAMUX to trigger the DMA transfer accordingly. The index
 * of the hardware request varies according  to the to SoC.
 */
typedef enum {
    EDMA_REQ_MUX_0_DISABLED = 0U,
    EDMA_REQ_SARADC_0_0 = 1U,
    EDMA_REQ_BCTU_2 = 2U,
    EDMA_REQ_ADCSD_0_0_EOC = 3U,
	EDMA_REQ_DECFIL_0_DRAIN = 4U,
	EDMA_REQ_DECFIL_0_FILL = 5U,
	EDMA_REQ_DSPI_0_0_CMD = 6U,
	EDMA_REQ_DSPI_0_0_RX = 7U,
	EDMA_REQ_DSPI_0_0_TX = 8U,
	EDMA_REQ_LINFLEX_0_0_RX = 9U,
	EDMA_REQ_LINFLEX_0_0_TX = 10U,
	EDMA_REQ_SIUL2_REQ0_0 = 11U,
	EDMA_REQ_SIUL2_REQ1 = 12U,
	EDMA_REQ_SIUL2_REQ6 = 13U,
	EDMA_REQ_EMIOS_0_CH8 = 15U,
	EDMA_REQ_EMIOS_0_CH9 = 16U,
	EDMA_REQ_EMIOS_0_CH10 = 17U,
	EDMA_REQ_EMIOS_0_CH11 = 18U,
	EDMA_REQ_EMIOS_0_CH12 = 19U,
	EDMA_REQ_ETPU2_A_CH0_0 = 20U,
	EDMA_REQ_ETPU2_A_CH1_0 = 21U,
	EDMA_REQ_ETPU2_A_CH2_0 = 22U,
	EDMA_REQ_ETPU2_A_CH3 = 23U,
	EDMA_REQ_ETPU2_A_CH4 = 24U,
	EDMA_REQ_ETPU2_A_CH15 = 25U,
	EDMA_REQ_ETPU2_A_CH16 = 26U,
	EDMA_REQ_ETPU2_A_CH17 = 27U,
	EDMA_REQ_ETPU2_A_CH18 = 28U,
	EDMA_REQ_ETPU2_A_CH19 = 29U,
	EDMA_REQ_ETPU2_A_CH30 = 30U,
	EDMA_REQ_ETPU2_A_CH31 = 31U,
	EDMA_REQ_ETPU2_B_CH0_0 = 32U,
	EDMA_REQ_ETPU2_B_CH1_0 = 33U,
	EDMA_REQ_ETPU2_B_CH2_0 = 34U,
	EDMA_REQ_ETPU2_B_CH13 = 35U,
	EDMA_REQ_ETPU2_B_CH14 = 36U,
	EDMA_REQ_ETPU2_B_CH15 = 37U,
	EDMA_REQ_ETPU2_B_CH16 = 38U,
	EDMA_REQ_ETPU2_B_CH17 = 39U,
	EDMA_REQ_ETPU2_B_CH28 = 40U,
	EDMA_REQ_ETPU2_B_CH29 = 41U,
	EDMA_REQ_ETPU2_B_CH30 = 42U,
	EDMA_REQ_ETPU2_B_CH31 = 43U,
	EDMA_REQ_SARADC_2_0 = 44U,
	EDMA_REQ_DSPI_2_0_CMD = 45U,
	EDMA_REQ_DSPI_2_0_RX = 46U,
	EDMA_REQ_DSPI_2_0_TX = 47U,
	EDMA_REQ_SIUL2_REQ4_0 = 48U,
	EDMA_REQ_FLEXCAN_0_0 = 49U,
	EDMA_REQ_REACM_CH0 = 50U,
	EDMA_REQ_REACM_CH3 = 51U,
	EDMA_REQ_REACM_CH6 = 52U,
	EDMA_REQ_REACM_CH7 = 53U,
	EDMA_REQ_MUX_0_ALWAYS_ENABLED_0 = 62U,
	EDMA_REQ_MUX_0_ALWAYS_ENABLED_1 = 63U,
	EDMA_REQ_MUX_1_DISABLED = 64U,
	EDMA_REQ_SARADC_2_1 = 65U,
	EDMA_REQ_BCTU_1_0 = 66U,
	EDMA_REQ_BCTU_3 = 67U,
	EDMA_REQ_DSPI_2_1_CMD = 68U,
	EDMA_REQ_DSPI_2_1_RX = 69U,
	EDMA_REQ_DSPI_2_1_TX = 70U,
	EDMA_REQ_DSPI_M0_CMD = 71U,
	EDMA_REQ_DSPI_M0_RX = 72U,
	EDMA_REQ_DSPI_M0_TX = 73U,
	EDMA_REQ_LINFLEX_M0_RX = 74U,
	EDMA_REQ_LINFLEX_M0_TX = 75U,
	EDMA_REQ_SIUL2_REQ2 = 76U,
	EDMA_REQ_SIUL2_REQ3 = 77U,
	EDMA_REQ_SIUL2_REQ7 = 78U,
	EDMA_REQ_FLEXCAN_2 = 79U,
	EDMA_REQ_EMIOS_0_CH13 = 80U,
	EDMA_REQ_EMIOS_0_CH14 = 81U,
	EDMA_REQ_EMIOS_0_CH15 = 82U,
	EDMA_REQ_EMIOS_0_CH16 = 83U,
	EDMA_REQ_EMIOS_0_CH17 = 84U,
	EDMA_REQ_ETPU2_A_CH5 = 85U,
	EDMA_REQ_ETPU2_A_CH6 = 86U,
	EDMA_REQ_ETPU2_A_CH7 = 87U,
	EDMA_REQ_ETPU2_A_CH8 = 88U,
	EDMA_REQ_ETPU2_A_CH9 = 89U,
	EDMA_REQ_ETPU2_A_CH20 = 90U,
	EDMA_REQ_ETPU2_A_CH21 = 91U,
	EDMA_REQ_ETPU2_A_CH22 = 92U,
	EDMA_REQ_ETPU2_A_CH23 = 93U,
	EDMA_REQ_ETPU2_A_CH24 = 94U,
	EDMA_REQ_ETPU2_B_CH3 = 95U,
	EDMA_REQ_ETPU2_B_CH4 = 96U,
	EDMA_REQ_ETPU2_B_CH5 = 97U,
	EDMA_REQ_ETPU2_B_CH6 = 98U,
	EDMA_REQ_ETPU2_B_CH7 = 99U,
	EDMA_REQ_ETPU2_B_CH18 = 100U,
	EDMA_REQ_ETPU2_B_CH19 = 101U,
	EDMA_REQ_ETPU2_B_CH20 = 102U,
	EDMA_REQ_ETPU2_B_CH21 = 103U,
	EDMA_REQ_ETPU2_B_CH22 = 104U,
	EDMA_REQ_BCTU_0_0 = 105U,
	EDMA_REQ_ADCSD_0_1_EOC = 106U,
	EDMA_REQ_LINFLEX_0_1_RX = 107U,
	EDMA_REQ_LINFLEX_0_1_TX = 108U,
	EDMA_REQ_LINFLEX_2_0_RX = 109U,
	EDMA_REQ_LINFLEX_2_0_TX = 110U,
	EDMA_REQ_SENT_0_0_RX_FAST = 111U,
	EDMA_REQ_SENT_0_0_RX_SLOW = 112U,
	EDMA_REQ_ETPU2_B_CH0_1 = 113U,
	EDMA_REQ_ETPU2_B_CH1_1 = 114U,
	EDMA_REQ_ETPU2_B_CH2_1 = 115U,
	EDMA_REQ_REACM_CH1 = 116U,
	EDMA_REQ_REACM_CH4 = 117U,
	EDMA_REQ_MUX_1_ALWAYS_ENABLED_0 = 127U,
	EDMA_REQ_MUX_2_DISABLED = 128U,
	EDMA_REQ_BCTU_0_1 = 129U,
	EDMA_REQ_ADCSD_2_EOC = 130U,
	EDMA_REQ_DECFIL_1_DRAIN = 131U,
	EDMA_REQ_DECFIL_1_FILL = 132U,
	EDMA_REQ_DSPI_4_CMD = 133U,
	EDMA_REQ_DSPI_4_RX = 134U,
	EDMA_REQ_DSPI_4_TX = 135U,
	EDMA_REQ_LINFLEX_2_1_RX = 136U,
	EDMA_REQ_LINFLEX_2_1_TX = 137U,
	EDMA_REQ_SENT_0_1_RX_FAST = 138U,
	EDMA_REQ_SENT_0_1_RX_SLOW = 139U,
	EDMA_REQ_SIUL2_REQ4_1 = 140U,
	EDMA_REQ_SIUL2_REQ5 = 141U,
	EDMA_REQ_FLEXCAN_0_1 = 142U,
	EDMA_REQ_EMIOS_0_CH18 = 143U,
	EDMA_REQ_EMIOS_0_CH19 = 144U,
	EDMA_REQ_EMIOS_0_CH20 = 145U,
	EDMA_REQ_EMIOS_0_CH21 = 146U,
	EDMA_REQ_EMIOS_0_CH22 = 147U,
    EDMA_REQ_EMIOS_0_CH23 = 148U,
	EDMA_REQ_ETPU2_A_CH10 = 149U,
	EDMA_REQ_ETPU2_A_CH11 = 150U,
	EDMA_REQ_ETPU2_A_CH12 = 151U,
	EDMA_REQ_ETPU2_A_CH13 = 152U,
	EDMA_REQ_ETPU2_A_CH14 = 153U,
	EDMA_REQ_ETPU2_A_CH25 = 154U,
	EDMA_REQ_ETPU2_A_CH26 = 155U,
	EDMA_REQ_ETPU2_A_CH27 = 156U,
	EDMA_REQ_ETPU2_A_CH28 = 157U,
	EDMA_REQ_ETPU2_A_CH29 = 158U,
	EDMA_REQ_ETPU2_B_CH8 = 159U,
	EDMA_REQ_ETPU2_B_CH9 = 160U,
	EDMA_REQ_ETPU2_B_CH10 = 161U,
	EDMA_REQ_ETPU2_B_CH11 = 162U,
	EDMA_REQ_ETPU2_B_CH12 = 163U,
	EDMA_REQ_ETPU2_B_CH23 = 164U,
	EDMA_REQ_ETPU2_B_CH24 = 165U,
	EDMA_REQ_ETPU2_B_CH25 = 166U,
	EDMA_REQ_ETPU2_B_CH26 = 167U,
	EDMA_REQ_ETPU2_B_CH27 = 168U,
	EDMA_REQ_SARADC_0_1 = 169U,
	EDMA_REQ_BCTU_1_1 = 170U,
	EDMA_REQ_DSPI_0_1_CMD = 171U,
	EDMA_REQ_DSPI_0_1_RX = 172U,
	EDMA_REQ_DSPI_0_1_TX = 173U,
	EDMA_REQ_SIUL2_REQ0_1 = 174U,
	EDMA_REQ_ETPU2_A_CH0_1 = 175U,
	EDMA_REQ_ETPU2_A_CH1_1 = 176U,
	EDMA_REQ_ETPU2_A_CH2_1 = 177U,
	EDMA_REQ_REACM_CH2 = 178U,
	EDMA_REQ_REACM_CH5 = 179U,
	EDMA_REQ_REACM_CH8 = 180U,
	EDMA_REQ_REACM_CH9 = 181U,
	EDMA_REQ_MUX_2_ALWAYS_ENABLED_0 = 191U,
	EDMA_REQ_MUX_3_DISABLED = 192U,
	EDMA_REQ_SARADC_1 = 193U,
	EDMA_REQ_SARADC_3 = 194U,
	EDMA_REQ_ADCSD_1_EOC = 195U,
	EDMA_REQ_DSPI_1_CMD = 196U,
	EDMA_REQ_DSPI_1_RX = 197U,
	EDMA_REQ_DSPI_1_TX = 198U,
	EDMA_REQ_DSPI_3_CMD = 199U,
	EDMA_REQ_DSPI_3_RX = 200U,
	EDMA_REQ_DSPI_3_TX = 201U,
	EDMA_REQ_DSPI_M1_CMD = 202U,
	EDMA_REQ_DSPI_M1_RX = 203U,
	EDMA_REQ_DSPI_M1_TX = 204U,
	EDMA_REQ_LINFLEX_1_RX = 205U,
	EDMA_REQ_LINFLEX_1_TX = 206U,
	EDMA_REQ_LINFLEX_3_RX = 207U,
	EDMA_REQ_LINFLEX_3_TX = 208U,
	EDMA_REQ_LINFLEX_M1_RX = 209U,
	EDMA_REQ_LINFLEX_M1_TX = 210U,
	EDMA_REQ_SENT_1_RX_FAST = 211U,
	EDMA_REQ_SENT_1_RX_SLOW = 212U,
	EDMA_REQ_SIPI_CH0 = 213U,
	EDMA_REQ_SIPI_CH1 = 214U,
	EDMA_REQ_SIPI_CH3 = 215U,
	EDMA_REQ_SIPI_CH4 = 216U,
	EDMA_REQ_FLEXCAN_1 = 217U,
	EDMA_REQ_FLEXCAN_3 = 218U,
	EDMA_REQ_EMIOS_1_CH8 = 219U,
	EDMA_REQ_EMIOS_1_CH9 = 220U,
	EDMA_REQ_EMIOS_1_CH10 = 221U,
	EDMA_REQ_EMIOS_1_CH11 = 222U,
	EDMA_REQ_EMIOS_1_CH12 = 223U,
	EDMA_REQ_EMIOS_1_CH13 = 224U,
	EDMA_REQ_EMIOS_1_CH14 = 225U,
	EDMA_REQ_EMIOS_1_CH15 = 226U,
	EDMA_REQ_EMIOS_1_CH16 = 227U,
	EDMA_REQ_EMIOS_1_CH17 = 228U,
	EDMA_REQ_EMIOS_1_CH18 = 229U,
	EDMA_REQ_EMIOS_1_CH19 = 230U,
	EDMA_REQ_EMIOS_1_CH20 = 231U,
	EDMA_REQ_EMIOS_1_CH21 = 232U,
	EDMA_REQ_EMIOS_1_CH22 = 233U,
	EDMA_REQ_EMIOS_1_CH23 = 234U
} dma_request_source_t;

/* EIM module features */

/* @brief The EIM channel types */
#define FEATURE_EIM_CH_TYPE_0 (0U)

/* @brief The EIM channel type structure */
#define FEATURE_EIM_CH_TYPE \
{                                \
FEATURE_EIM_CH_TYPE_0,         /* Channel type corresponding to channel 0  */       \
}

/* @brief The EIM number of data words for each channel type  */
#define FEATURE_EIM_CH_TYPE_0_NUM_OF_DATA_WORDS (2U)

/* @brief The EIM WORD0 bitfields for each channel type  */
#define FEATURE_EIM_CH_TYPE_0_WORD0_MASK      0xFE000000u
#define FEATURE_EIM_CH_TYPE_0_WORD0_SHIFT     25u
#define FEATURE_EIM_CH_TYPE_0_WORD0_WRITE(x)  (((uint32_t)(((uint32_t)(x))<<FEATURE_EIM_CH_TYPE_0_WORD0_SHIFT))&FEATURE_EIM_CH_TYPE_0_WORD0_MASK)
#define FEATURE_EIM_CH_TYPE_0_WORD0_READ(x)   (((uint32_t)(((uint32_t)(x))&FEATURE_EIM_CH_TYPE_0_WORD0_MASK))>>FEATURE_EIM_CH_TYPE_0_WORD0_SHIFT)

/* @brief The EIM WORD1 bitfields for each channel type  */
#define FEATURE_EIM_CH_TYPE_0_WORD1_MASK      0xFFFFFFFFu
#define FEATURE_EIM_CH_TYPE_0_WORD1_SHIFT     0u
#define FEATURE_EIM_CH_TYPE_0_WORD1_WRITE(x)  (((uint32_t)(((uint32_t)(x))<<FEATURE_EIM_CH_TYPE_0_WORD1_SHIFT))&FEATURE_EIM_CH_TYPE_0_WORD1_MASK)
#define FEATURE_EIM_CH_TYPE_0_WORD1_READ(x)   (((uint32_t)(((uint32_t)(x))&FEATURE_EIM_CH_TYPE_0_WORD1_MASK))>>FEATURE_EIM_CH_TYPE_0_WORD1_SHIFT)

/* @brief The EIM WORD2 mask for each channel type  */
#define FEATURE_EIM_CH_TYPE_0_WORD2_MASK      0xFFFFFFFFu
#define FEATURE_EIM_CH_TYPE_0_WORD2_SHIFT     0u
#define FEATURE_EIM_CH_TYPE_0_WORD2_WRITE(x)  (((uint32_t)(((uint32_t)(x))<<FEATURE_EIM_CH_TYPE_0_WORD2_SHIFT))&FEATURE_EIM_CH_TYPE_0_WORD2_MASK)
#define FEATURE_EIM_CH_TYPE_0_WORD2_READ(x)   (((uint32_t)(((uint32_t)(x))&FEATURE_EIM_CH_TYPE_0_WORD2_MASK))>>FEATURE_EIM_CH_TYPE_0_WORD2_SHIFT)

/* BCTU (Body Cross Triggering Unit) module features */
/*! @brief Number of ADC instances which can be triggered by BCTU */
#define FEATURE_BCTU_NUM_ADC            (4U)
#define FEATURE_BCTU_HAS_CCP            (1U)

/* ADC module features */
#define FEATURE_ADC_HAS_CTU_TRIGGER_MODE (1)
#define FEATURE_ADC_HAS_EXT_TRIGGER      (1)
#define FEATURE_ADC_HAS_INJ_TRIGGER_SEL  (0)
#define FEATURE_ADC_HAS_THRHLR_ARRAY     (1)

#define FEATURE_ADC_HAS_CWSELR_UNROLLED  (1)
#define FEATURE_ADC_HAS_CWSELR0          (1)
#define FEATURE_ADC_HAS_CWSELR1          (1)
#define FEATURE_ADC_HAS_CWSELR2          (1)
#define FEATURE_ADC_HAS_CWSELR3          (1)
#define FEATURE_ADC_HAS_CWSELR4          (1)
#define FEATURE_ADC_HAS_CWSELR5          (0)
#define FEATURE_ADC_HAS_CWSELR6          (0)
#define FEATURE_ADC_HAS_CWSELR7          (0)
#define FEATURE_ADC_HAS_CWSELR8          (1)
#define FEATURE_ADC_HAS_CWSELR9          (1)
#define FEATURE_ADC_HAS_CWSELR10         (1)
#define FEATURE_ADC_HAS_CWSELR11         (1)

#define ADC_PRESAMPLE_VSS_HV         (ADC_PRESAMPLE_SRC0)    /* Presampling from High voltage ground for ADC*/
#define ADC_PRESAMPLE_VDD_HV_DIV_8   (ADC_PRESAMPLE_SRC1)    /* Presampling from ower High voltage Supply for ADC, SGEN divided by 8 */
#define ADC_PRESAMPLE_VSS_HV_ADRE1   (ADC_PRESAMPLE_SRC2)    /* Presampling from High voltage ground for ADC/TSENS */
#define ADC_PRESAMPLE_VDD_HV_ADRE1   (ADC_PRESAMPLE_SRC3)    /* Presampling from High voltage reference for ADC/TSENS */

#define ADC_CLOCKS                    {ADC0_CLK, ADC1_CLK, ADC2_CLK, ADC3_CLK}

#define FEATURE_ADC_BAD_ACCESS_PROT_CHANNEL  (1)
                                      /* 31-28   3-0  63-60  35-32 95-92  68-65
                                          \_/    \_/   \_/    \_/   \_/    \_/
                                           |......|     |......|     |......| */
#define FEATURE_ADC_CHN_AVAIL_BITMAP   {{0xFFFFFFFFu, 0x0000007Fu, 0x00000000u}, /* ADC0 */ \
                                        {0xFFFFFFFFu, 0x0000003Fu, 0x00000000u}, /* ADC1 */ \
                                        {0xFFFFFFFFu, 0x0000000Fu, 0x00000000u}, /* ADC2 */ \
                                        {0x001FFFFFu, 0x0000003Du, 0xFFFFFFFFu}} /* ADC3 */

#define FEATURE_ADC_BAD_ACCESS_PROT_FEATURE  (1)
#define FEATURE_ADC_FEAT_AVAIL_BITMAP  {0xFFFFFFFCu,  /* ADC0 */ \
                                        0xFFFFFFFEu,  /* ADC1 */ \
                                        0xFFFFFFFEu,  /* ADC2 */ \
                                        0xFFFFFFFFu}  /* ADC3 */

/* @brief Default values for sample time. */
#define ADC_DEF_SAMPLE_TIME_0           (0x14U)
#define ADC_DEF_SAMPLE_TIME_1           (0x14U)
#define ADC_DEF_SAMPLE_TIME_2           (0x14U)

/* SDADC module features */
/*! @brief SDADC module has instance 0 */
#define FEATURE_SDADC_HAS_INSTANCE_0      (1u)
/*! @brief SDADC clocks */
#define SDADC_CLOCKS                      {ADCSD0_CLK, ADCSD1_CLK, ADCSD2_CLK}
/*! @brief SUIL IMCR register number for SDADC input trigger configuring */
#define TRIGGER_IMCR_REGISTER_NUMBERS     {456u, 457u, 458u}
/*! @brief SUIL IMCR register number for SDADC DMA/Interrupt gating configuring */
#define GATE_IMCR_REGISTER_NUMBERS        {448u, 449u, 450u}
/* @brief Max of SDADC clock frequency */
#define SDADC_CLOCK_FREQ_MAX_RUNTIME     (16000000u)
/* @brief Min of SDADC clock frequency */
#define SDADC_CLOCK_FREQ_MIN_RUNTIME     (4000000u)
/*! @brief SDADC instances number */
#define FEATURE_SDADC_HAS_INSTANCE_NUMBER    (3u)
/*! @brief SDADC initial gain error table */
#define SDADC_GAIN_ERROR   {65536, 65536, 65536}


/* ADC PAL features */
#define ADC_PAL_BCTU_LIST_LAST_IRQn      				BCTU0_LIST_IRQn
#define ADC_PAL_BCTU_CONV_UPDATE_IRQ_UNROLLED           (1)


/* SIUL2 module feature */
/*! @brief SIUL2 module external interrupt support DMA */
#define FEATURE_SIUL2_EXTERNAL_INT_SUPPORT_DMA          (1)
/*! @brief SIUL2 module support Analog Pad */
#define FEATURE_SIUL2_HAS_ANALOG_PAD                    (1)
/*! @brief SIUL2 module Slew rate combine with Drive strength */
#define FEATURE_SIUL2_HAS_SLEW_RATE_WITH_DRIVE_STRENGTH (1)
/*! @brief SIUL2 module input source select bit width */
#define FEATURE_SIUL2_INPUT_SOURCE_SELECT_WIDTH         SIUL2_IMCR_SSS_WIDTH
/*! @brief SIUL2 module input mux numbers */
#define FEATURE_SIUL2_INPUT_MUX_WIDTH                   (16U)
/*! @brief SIUL2 module has specific pull register */
#define FEATURE_SIUL2_HAS_SPECIFIC_PULL_REGISTER        (1U)
/*! @brief SIUL2 module has input level selection */
#define FEATURE_SIUL2_HAS_INPUT_LEVEL_SELECTION         (1U)
/*! @brief SIUL2 module has output drive control */
#define FEATURE_SIUL2_HAS_OUTPUT_DRIVE_CONTROL          (1U)
/*! @brief SIUL2 module has weak pull enable */
#define FEATURE_SIUL2_HAS_WEAK_PULL_ENABLE              (1U)
/*! @brief SIUL2 module re-define MSCR[OERC] bit */
#define SIUL2_MSCR_SRC                                  SIUL2_MSCR_OERC

/** GPIO - Register Layout Typedef */
typedef struct {
  __IO uint16_t PGPDO;          /**< SIUL2 Parallel GPIO Pad Data Out Register, array offset: 0x1700, array step: 0x2 */
       uint16_t RESERVED_PGPDO[SIUL2_PGPDO_COUNT - 1U];
       uint8_t RESERVED_13[32];
  __I  uint16_t PGPDI;          /**< SIUL2 Parallel GPIO Pad Data In Register, array offset: 0x1740, array step: 0x2 */
} GPIO_Type;

 /** Number of instances of the SIUL2 module. */
#define GPIO_INSTANCE_COUNT (10u)

/* SIUL2 - Peripheral instance base addresses */
/** Peripheral SIUL2 base address */
#define GPIO_BASE                                (0xFFFC1700u)
/** Peripheral SIUL2 base pointer */
#define GPIO                                     ((GPIO_Type *)GPIO_BASE)

 /* GPIO - Peripheral instance base addresses */
/** Peripheral PTA base address */
#define PTA_BASE                                 (0xFFFC1700u)
/** Peripheral PTA base pointer */
#define PTA                                      ((GPIO_Type *)PTA_BASE)
/** Peripheral PTB base address */
#define PTB_BASE                                 (0xFFFC1702u)
/** Peripheral PTB base pointer */
#define PTB                                      ((GPIO_Type *)PTB_BASE)
/** Peripheral PTC base address */
#define PTC_BASE                                 (0xFFFC1704u)
/** Peripheral PTC base pointer */
#define PTC                                      ((GPIO_Type *)PTC_BASE)
/** Peripheral PTD base address */
#define PTD_BASE                                 (0xFFFC1706u)
/** Peripheral PTD base pointer */
#define PTD                                      ((GPIO_Type *)PTD_BASE)
/** Peripheral PTE base address */
#define PTE_BASE                                 (0xFFFC1708u)
/** Peripheral PTE base pointer */
#define PTE                                      ((GPIO_Type *)PTE_BASE)
/** Peripheral PTF base address */
#define PTF_BASE                                 (0xFFFC170Au)
/** Peripheral PTF base pointer */
#define PTF                                      ((GPIO_Type *)PTF_BASE)
/** Peripheral PTG base address */
#define PTG_BASE                                 (0xFFFC170Cu)
/** Peripheral PTG base pointer */
#define PTG                                      ((GPIO_Type *)PTG_BASE)
/** Peripheral PTH base address */
#define PTH_BASE                                 (0xFFFC170Eu)
/** Peripheral PTH base pointer */
#define PTH                                      ((GPIO_Type *)PTH_BASE)
/** Peripheral PTI base address */
#define PTI_BASE                                 (0xFFFC1710u)
/** Peripheral PTI base pointer */
#define PTI                                      ((GPIO_Type *)PTI_BASE)
/** Peripheral PTJ base address */
#define PTJ_BASE                                 (0xFFFC1712u)
/** Peripheral PTJ base pointer */
#define PTJ                                      ((GPIO_Type *)PTJ_BASE)
/** Peripheral PTK base address */
#define PTK_BASE                                 (0xFFFC1714u)
/** Peripheral PTK base pointer */
#define PTK                                      ((GPIO_Type *)PTJ_BASE)
/** Peripheral PTL base address */
#define PTL_BASE                                 (0xFFFC1716u)
/** Peripheral PTL base pointer */
#define PTL                                      ((GPIO_Type *)PTJ_BASE)
/** Peripheral PTW base address */
#define PTW_BASE                                 (0xFFFC1718u)
/** Peripheral PTW base pointer */
#define PTW                                      ((GPIO_Type *)PTJ_BASE)
/** Peripheral PTX base address */
#define PTX_BASE                                 (0xFFFC171Au)
/** Peripheral PTX base pointer */
#define PTX                                      ((GPIO_Type *)PTJ_BASE)
/** Peripheral PTY base address */
#define PTY_BASE                                 (0xFFFC171Cu)
/** Peripheral PTY base pointer */
#define PTY                                      ((GPIO_Type *)PTJ_BASE)
/** Peripheral PTZ base address */
#define PTZ_BASE                                 (0xFFFC171Eu)
/** Peripheral PTZ base pointer */
#define PTZ                                      ((GPIO_Type *)PTJ_BASE)

/** Array initializer of GPIO peripheral base addresses */
#define GPIO_BASE_ADDRS                          { PTA_BASE, PTB_BASE, PTC_BASE, PTD_BASE, \
                                                   PTE_BASE, PTF_BASE, PTG_BASE, PTH_BASE, \
                                                   PTI_BASE, PTJ_BASE, PTK_BASE, PTL_BASE, \
                                                   PTW_BASE, PTX_BASE, PTY_BASE, PTZ_BASE }
/** Array initializer of GPIO peripheral base pointers */
#define GPIO_BASE_PTRS                           { PTA, PTB, PTC, PTD, PTE, PTF, PTG, PTH, \
                                                   PTI, PTJ, PTK, PTL, PTW, PTX, PTY, PTZ }

/** PORT - Register Layout Typedef */
typedef struct {
  __IO  uint32_t MSCR[16];
} PORT_Type;

#define SIUL2_MSCR_BASE                          (SIUL2->MSCR)
#define SIUL2_IMCR_BASE                          (SIUL2->IMCR)
#define PORTA                                    ((PORT_Type *)(SIUL2_MSCR_BASE+0x00))
#define PORTB                                    ((PORT_Type *)(SIUL2_MSCR_BASE+0x10))
#define PORTC                                    ((PORT_Type *)(SIUL2_MSCR_BASE+0x20))
#define PORTD                                    ((PORT_Type *)(SIUL2_MSCR_BASE+0x30))
#define PORTE                                    ((PORT_Type *)(SIUL2_MSCR_BASE+0x40))
#define PORTF                                    ((PORT_Type *)(SIUL2_MSCR_BASE+0x50))
#define PORTG                                    ((PORT_Type *)(SIUL2_MSCR_BASE+0x60))
#define PORTH                                    ((PORT_Type *)(SIUL2_MSCR_BASE+0x70))
#define PORTI                                    ((PORT_Type *)(SIUL2_MSCR_BASE+0x80))
#define PORTJ                                    ((PORT_Type *)(SIUL2_MSCR_BASE+0x90))
#define PORTK                                    ((PORT_Type *)(SIUL2_MSCR_BASE+0xA0))
#define PORTL                                    ((PORT_Type *)(SIUL2_MSCR_BASE+0xB0))
#define PORTW                                    ((PORT_Type *)(SIUL2_MSCR_BASE+0xC0))
#define PORTX                                    ((PORT_Type *)(SIUL2_MSCR_BASE+0xD0))
#define PORTY                                    ((PORT_Type *)(SIUL2_MSCR_BASE+0xE0))
#define PORTZ                                    ((PORT_Type *)(SIUL2_MSCR_BASE+0xF0))
#define PORT_BASE_PTRS                           { PORTA, PORTB, PORTC, PORTD, PORTE, PORTF, \
                                                   PORTG, PORTH, PORTI, PORTJ, PORTK, PORTL, \
                                                   PORTW, PORTX, PORTY, PORTZ }

/** Define specific pins, which have general purpose pull */
#define PTY0                                     (0U)
#define PTY1                                     (1U)
#define PTY2                                     (2U)
#define PTY3                                     (3U)
#define PTZ0                                     (4U)
#define PTZ1                                     (5U)
#define PTZ2                                     (6U)
#define PTZ3                                     (7U)
#define PTZ4                                     (8U)
#define PTZ5                                     (9U)
#define PTZ6                                     (10U)
#define PTZ7                                     (11U)
#define PTZ8                                     (12U)
#define PTZ9                                     (13U)
#define PTZ10                                    (14U)
#define PTZ11                                    (15U)
#define PTZ12                                    (16U)
#define PTZ13                                    (17U)
#define PTZ14                                    (18U)
#define PTZ15                                    (19U)

/* DSPI module feature */

/* Define the PCS allocation for each DSPI/SPI module */
#define FEATURE_DSPI_0_PCS_NUMBER  6
#define FEATURE_DSPI_1_PCS_NUMBER  5
#define FEATURE_DSPI_2_PCS_NUMBER  4
#define FEATURE_DSPI_3_PCS_NUMBER  6
#define FEATURE_DSPI_4_PCS_NUMBER  6
#define FEATURE_DSPI_M0_PCS_NUMBER 4
#define FEATURE_DSPI_M1_PCS_NUMBER 4

#define FEATURE_DSPI_PCS_MAPPING {FEATURE_DSPI_0_PCS_NUMBER, FEATURE_DSPI_1_PCS_NUMBER, FEATURE_DSPI_2_PCS_NUMBER, \
                                  FEATURE_DSPI_3_PCS_NUMBER, FEATURE_DSPI_4_PCS_NUMBER, FEATURE_DSPI_M0_PCS_NUMBER, FEATURE_DSPI_M1_PCS_NUMBER}

/* Define the instance realocation */
#define FEATURE_DSPI_INSTANCES  { DSPI_0, DSPI_1, DSPI_2, DSPI_3, DSPI_4, DSPI_M_0, DSPI_M_1 }

/* Define the initial values for state structures */
#define FEATURE_DSPI_INITIAL_STATE {NULL, NULL, NULL, NULL, NULL, NULL, NULL}

/* Define clock sources for SPI/DSPI instances */
#define FEATURE_DSPI_CLOCK_MAPPING {DSPI0_CLK, DSPI1_CLK, DSPI2_CLK, DSPI3_CLK, DSPI4_CLK, DSPIM0_CLK, DSPIM1_CLK}

/* Define margins of instances interval */
#define DSPI_LOWEST_INSTANCE  0U
#define DSPI_HIGHEST_INSTANCE 6U

/* Define FIFO size */
#define DSPI_FIFO_SIZE 4U

/* Each interrupt flag has it's own interrupt index */
#define FEATURES_DSPI_HAS_INDEPENDENT_INTERRUPTS 0U

/* On this platform DSPI in extended mode is supported */
#define FEATURE_DSPI_HAS_EXTENDED_MODE 0U

/* Define number of DSPI instances */
#define SPI_INSTANCE_COUNT         0U

/* Define interrupt vector for dspi */
#define FEATURES_DSPI_SEND_INTERUPT_VECTOR    { DSPI0_TFFF_IRQn, DSPI1_TFFF_IRQn, DSPI2_TFFF_IRQn, \
                                                DSPI3_TFFF_IRQn, DSPI4_TFFF_IRQn, DSPI_M0_TFFF_IRQn, DSPI_M1_TFFF_IRQn}

#define FEATURES_DSPI_RECEIVE_INTERUPT_VECTOR { DSPI0_RFDF_IRQn, DSPI1_RFDF_IRQn, DSPI2_RFDF_IRQn, \
                                                DSPI3_RFDF_IRQn, DSPI4_RFDF_IRQn, DSPI_M0_RFDF_IRQn, DSPI_M1_RFDF_IRQn}

#define FEATURES_DSPI_FAULT_INTERUPT_VECTOR   { DSPI0_ERR_IRQn, DSPI1_ERR_IRQn, DSPI2_ERR_IRQn, \
                                                DSPI3_ERR_IRQn, DSPI4_ERR_IRQn, DSPI_M0_ERR_IRQn, DSPI_M1_ERR_IRQn}
/* SRX module features */

/* Has nibble length limit */
#define FEATURE_SRX_HAS_NIB_LEN_VAR_LIMIT

/* Mappings */
#define FEATURE_SRX_FAST_DMA_REQS {EDMA_REQ_SENT_0_0_RX_FAST, EDMA_REQ_SENT_1_RX_FAST}
#define FEATURE_SRX_SLOW_DMA_REQS {EDMA_REQ_SENT_0_0_RX_SLOW, EDMA_REQ_SENT_1_RX_SLOW}
#define FEATURE_SRX_CLOCK_NAMES {SENT0_CLK, SENT1_CLK}
#define FEATURE_SRX_IRQS {{{SENT0_CH0_FAST_IRQn, SENT0_CH0_SLOW_IRQn, SENT0_CH0_ERR_IRQn}, \
                           {SENT0_CH1_FAST_IRQn, SENT0_CH1_SLOW_IRQn, SENT0_CH1_ERR_IRQn}, \
                           {SENT0_CH2_FAST_IRQn, SENT0_CH2_SLOW_IRQn, SENT0_CH2_ERR_IRQn}}, \
                          {{SENT1_CH0_FAST_IRQn, SENT1_CH0_SLOW_IRQn, SENT1_CH0_ERR_IRQn}, \
                           {SENT1_CH1_FAST_IRQn, SENT1_CH1_SLOW_IRQn, SENT1_CH1_ERR_IRQn}, \
                           {SENT1_CH2_FAST_IRQn, SENT1_CH2_SLOW_IRQn, SENT1_CH2_ERR_IRQn}}}

/* ENET module features */

/*! @brief ENET peripheral clock names */
#define FEATURE_ENET_CLOCK_NAMES { ENET0_CLK }

/*! @brief The transmission interrupts */
#define FEATURE_ENET_TX_IRQS        FEC_TX_IRQS
/*! @brief The reception interrupts */
#define FEATURE_ENET_RX_IRQS        FEC_RX_IRQS
/*! @brief The error interrupts */
#define FEATURE_ENET_ERR_IRQS       FEC_ERR_IRQS

/*! @brief The maximum supported frequency for MDC, in Hz. */
#define FEATURE_ENET_MDC_MAX_FREQUENCY 2500000U

/*! @brief Minimum hold time on the MDIO output, in nanoseconds. */
#define FEATURE_ENET_MDIO_MIN_HOLD_TIME_NS 10U

/*! @brief Definitions used for aligning the data buffers */
#define FEATURE_ENET_BUFF_ALIGNMENT      (16UL)
/*! @brief Definitions used for aligning the buffer descriptors */
#define FEATURE_ENET_BUFFDESCR_ALIGNMENT (64UL)

/*! @brief Has receive frame parser feature. */
#define FEATURE_ENET_HAS_RECEIVE_PARSER (0)

/*! @brief Has enhanced buffer descriptors programming model. */
#define FEATURE_ENET_HAS_ENHANCED_BD    (0)

/*! @brief Default configuration for the PHY interface */
#define FEATURE_ENET_DEFAULT_PHY_IF     ENET_RMII_MODE

/*! @brief Has adjustable timer enabling IEEE 1588 support */
#define FEATURE_ENET_HAS_ADJUSTABLE_TIMER (0)

/*! @brief Has support for configuring the width of the output compare pulse */
#define FEATURE_ENET_HAS_TIMER_PULSE_WIDTH_CONTROL (0)

/*! @brief The number of available receive and transmit buffer descriptor rings */
#define FEATURE_ENET_RING_COUNT             (1U)

/*! @brief The number of available VLAN priority compare values per class */
#define FEATURE_ENET_CLASS_MATCH_CMP_COUNT  (0U)

/*! @brief Has credit-based shaper */
#define FEATURE_ENET_HAS_CBS                (0)

/*! @brief Has time-based shaper */
#define FEATURE_ENET_HAS_TBS                (0)

/*! @brief Has interrupt coalescing */
#define FEATURE_ENET_HAS_INTCOAL            (0)

/*! @brief Has RGMII mode for data interface */
#define FEATURE_ENET_HAS_RGMII              (0)

/*! @brief Has 1000-Mbit/s speed mode */
#define FEATURE_ENET_HAS_SPEED_1000M        (0)

/*! @brief Has Receive and Transmit accelerator */
#define FEATURE_ENET_HAS_ACCELERATOR		(0)

/*! @brief Has Sleep and Wakeup functionalities */
#define FEATURE_ENET_HAS_SLEEP_WAKEUP		(0)

/*! @brief Has configurable FIFO thresholds */
#define FEATURE_ENET_HAS_CONFIG_FIFO_THRESHOLDS	(0)

/*! @brief RX Configuration features */
#define FEATURE_ENET_HAS_RX_CONFIG					(1)
#define FEATURE_ENET_RX_CONFIG_PAYLOAD_LEN_CHECK	(0)
#define FEATURE_ENET_RX_CONFIG_STRIP_CRC_FIELD		(0)
#define FEATURE_ENET_RX_CONFIG_REMOVE_PADDING		(0)
#define FEATURE_ENET_RX_CONFIG_FORWARD_PAUSE_FRAMES	(0)

/*! @brief TX Configuration features */
#define FEATURE_ENET_HAS_TX_CONFIG	(0)

/*! @brief Offset of the MIB block counters area in bytes */
#define FEATURE_ENET_MIB_COUNTERS_OFFSET	(0x200U)
/*! @brief Size of the MIB block counters area in words */
#define FEATURE_ENET_MIB_COUNTERS_SIZE		(0x80U)

/*! @brief The minimum size of the Tx ring */
#define FEATURE_ENET_TX_MIN_RING_SIZE       (2U)

#define ENET_Type FEC_Type

#define ENET_INSTANCE_COUNT	FEC_INSTANCE_COUNT
#define ENET_BASE_PTRS		FEC_BASE_PTRS

#define ENET_EIR_BABR_MASK	FEC_EIR_BABR_MASK
#define ENET_EIR_BABT_MASK	FEC_EIR_BABT_MASK
#define ENET_EIR_GRA_MASK	FEC_EIR_GRA_MASK
#define ENET_EIR_TXF_MASK	FEC_EIR_TXF_MASK
#define ENET_EIR_TXB_MASK	FEC_EIR_TXB_MASK
#define ENET_EIR_RXF_MASK	FEC_EIR_RXF_MASK
#define ENET_EIR_RXB_MASK	FEC_EIR_RXB_MASK
#define ENET_EIR_MII_MASK	FEC_EIR_MII_MASK
#define ENET_EIR_EBERR_MASK	FEC_EIR_EBERR_MASK
#define ENET_EIR_LC_MASK	FEC_EIR_LC_MASK
#define ENET_EIR_RL_MASK	FEC_EIR_RL_MASK
#define ENET_EIR_UN_MASK	FEC_EIR_UN_MASK

#define ENET_MMFR_DATA_MASK		FEC_MMFR_DATA_MASK
#define ENET_MMFR_ST			FEC_MMFR_ST
#define ENET_MMFR_OP			FEC_MMFR_OP
#define ENET_MMFR_PA			FEC_MMFR_PA
#define ENET_MMFR_RA			FEC_MMFR_RA
#define ENET_MMFR_TA			FEC_MMFR_TA
#define ENET_MMFR_DATA			FEC_MMFR_DATA
#define ENET_PAUR_PADDR2_MASK	FEC_PAUR_PADDR2_MASK
#define ENET_PAUR_PADDR2_SHIFT	FEC_PAUR_PADDR2_SHIFT
#define ENET_ECR_ETHEREN_MASK	FEC_ECR_ETHER_EN_MASK
#define ENET_TDAR_TDAR_MASK		FEC_TDAR_TDAR_MASK
#define ENET_RDAR_RDAR_MASK		FEC_RDAR_RDAR_MASK
#define ENET_ECR_RESET_MASK		FEC_ECR_RESET_MASK

/* CAN module features */

/* @brief Frames available in Rx FIFO flag shift */
#define FEATURE_CAN_RXFIFO_FRAME_AVAILABLE  (5U)
/* @brief Rx FIFO warning flag shift */
#define FEATURE_CAN_RXFIFO_WARNING          (6U)
/* @brief Rx FIFO overflow flag shift */
#define FEATURE_CAN_RXFIFO_OVERFLOW         (7U)
/* @brief Maximum number of Message Buffers supported for payload size 8 for CAN0 */
#define FEATURE_CAN0_MAX_MB_NUM             (96U)
/* @brief Maximum number of Message Buffers supported for payload size 8 for CAN1 */
#define FEATURE_CAN1_MAX_MB_NUM             (96U)
/* @brief Maximum number of Message Buffers supported for payload size 8 for CAN2 */
#define FEATURE_CAN2_MAX_MB_NUM             (64U)
/* @brief Maximum number of Message Buffers supported for payload size 8 for CAN3 */
#define FEATURE_CAN3_MAX_MB_NUM             (64U)
/* @brief Maximum number of Message Buffers supported for payload size 8 for any of the CAN instances */
#define FEATURE_CAN_MAX_MB_NUM              (96U)
/* @brief Array of maximum number of Message Buffers supported for payload size 8 for all the CAN instances */
#define FEATURE_CAN_MAX_MB_NUM_ARRAY        { FEATURE_CAN0_MAX_MB_NUM, \
                                              FEATURE_CAN1_MAX_MB_NUM, \
                                              FEATURE_CAN2_MAX_MB_NUM, \
                                              FEATURE_CAN3_MAX_MB_NUM  }
/* @brief Has PE clock source select (bit field CAN_CTRL1[CLKSRC]). */
#define FEATURE_CAN_HAS_PE_CLKSRC_SELECT            (0)
/* @brief Has Pretending Networking mode */
#define FEATURE_CAN_HAS_PRETENDED_NETWORKING        (0)
/* @brief Has Stuff Bit Count Enable Bit */
#define FEATURE_CAN_HAS_STFCNTEN_ENABLE             (0)
/* @brief Has ISO CAN FD Enable Bit */
#define FEATURE_CAN_HAS_ISOCANFDEN_ENABLE           (0)
/* @brief Has Message Buffer Data Size Region 1 */
#define FEATURE_CAN_HAS_MBDSR1                      (0)
/* @brief Has Message Buffer Data Size Region 2 */
#define FEATURE_CAN_HAS_MBDSR2                      (0)
/* @brief Has Supervisor Mode MCR[SUPV]*/
#define FEATURE_CAN_HAS_SUPV             	        (1)
/* @brief Has DMA enable (bit field MCR[DMA]). */
#define FEATURE_CAN_HAS_DMA_ENABLE                  (1)
/* @brief Maximum number of Message Buffers IRQs */
#define FEATURE_CAN_MB_IRQS_MAX_COUNT       (4U)
/* @brief Has Wake Up Irq channels (CAN_Wake_Up_IRQS_CH_COUNT > 0u) */
#define FEATURE_CAN_HAS_WAKE_UP_IRQ         (0U)
/* @brief Message Buffers IRQs */
#define FEATURE_CAN_MB_IRQS                 { CAN_ORed_00_15_MB_IRQS, \
                                              CAN_ORed_16_31_MB_IRQS, \
                                              CAN_ORed_32_63_MB_IRQS, \
                                              CAN_ORed_64_95_MB_IRQS  }
/*! @brief FlexCAN Embedded RAM address offset */
#define FEATURE_CAN_RAM_OFFSET              (0x00000080u)
/* @brief Has Self Wake Up mode */
#define FEATURE_CAN_HAS_SELF_WAKE_UP        (0)
/* @brief Has Flexible Data Rate */
#define FEATURE_CAN_HAS_FD                  (0)
/* @brief Clock name for the PE oscillator clock source */
#define FEATURE_CAN_PE_OSC_CLK_NAME         XOSC_CLK
/* @bried FlexCAN has Detection And Correction of Memory Errors */
#define FEATURE_CAN_HAS_MEM_ERR_DET			(1)

/* TIMING_PAL module features */

/* @brief PIT default interrupt handler */
#define PIT0_Ch0_IRQHandler   PIT0_TFLG0_IRQHandler

#define PIT0_Ch1_IRQHandler   PIT0_TFLG1_IRQHandler

#define PIT0_Ch2_IRQHandler   PIT0_TFLG2_IRQHandler

#define PIT0_Ch3_IRQHandler   PIT0_TFLG3_IRQHandler

#define PIT0_Ch4_IRQHandler   PIT0_TFLG4_IRQHandler

#define PIT0_Ch5_IRQHandler   PIT0_TFLG5_IRQHandler

#define PIT0_Ch6_IRQHandler   PIT0_TFLG6_IRQHandler

#define PIT0_Ch7_IRQHandler   PIT0_TFLG7_IRQHandler

#define PIT1_Ch0_IRQHandler   PIT1_TFLG0_IRQHandler

#define PIT1_Ch1_IRQHandler   PIT1_TFLG1_IRQHandler

#define PIT1_Ch2_IRQHandler   PIT1_TFLG2_IRQHandler

#define PIT1_Ch3_IRQHandler   PIT1_TFLG3_IRQHandler
/* TDM module features */

/*! @brief TDR count */
#define TDR_COUNT 6U

/* PASS module features */

/* @brief Has Region 0 */
#define FEATURE_PASS_HAS_RL0                  (0U)
/* @brief Has Region 1 */
#define FEATURE_PASS_HAS_RL1                  (1U)
/* @brief Has Region 2 */
#define FEATURE_PASS_HAS_RL2                  (2U)

#endif /* MPC5745R_FEATURES_H */

/*******************************************************************************
 * EOF
 ******************************************************************************/
