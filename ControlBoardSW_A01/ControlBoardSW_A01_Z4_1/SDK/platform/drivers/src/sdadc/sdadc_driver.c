/*
 * Copyright 2018 NXP
 * All rights reserved.
 *
 * THIS SOFTWARE IS PROVIDED BY NXP "AS IS" AND ANY EXPRESSED OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL NXP OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @page misra_violations MISRA-C:2012 violations
 *
 * @section [global]
 * Violates MISRA 2012 Required Rule 1.3,  Taking address of near auto variable.
 * The code is not dynamically linked. An absolute stack address is obtained
 * when taking the address of the near auto variable.
 *
 * @section [global]
 * Violates MISRA 2012 Advisory Rule 8.7, External could be made static.
 * Function is defined for usage by application code.
 *
 * @section [global]
 * Violates MISRA 2012 Advisory Rule 11.4, Conversion between a pointer and integer type.
 * The cast is required to initialize a pointer with an unsigned long define, representing an address.
 *
 * @section [global]
 * Violates MISRA 2012 Required Rule 11.6, Cast from unsigned int to pointer.
 * The cast is required to initialize a pointer with an unsigned long define, representing an address.
 *
 * @section [global]
 * Violates MISRA 2012 Required Rule 10.3, Expression assigned to a narrower or different essential type.
 * The cast is required to perform a conversion between an unsigned integer and an enum type with
 * many values.
 *
 * @section [global]
 * Violates MISRA 2012 Required Rule 10.4, Mismatched essential type categories for binary operator.
 * This is required by the average calculation of the data conversion.
 *
 * @section [global]
 * Violates MISRA 2012 Advisory Rule 10.5, Impermissible cast; cannot cast from 'essentially Boolean'
 * to 'essentially unsigned'. This is required by the conversion of a bool into a bit.
 *
 * @section [global]
 * Violates MISRA 2012 Required Rule 10.6, Composite expression assigned to a wider essential type
 * This is required by the conversion of a bit-field of a register into int16_t type.
 *
 * @section [global]
 * Violates MISRA 2012 Required Rule 10.7,  Composite expression with smaller essential type than other operand.
 * The expression is safe as the calculation cannot overflow.
 *
 * @section [global]
 * Violates MISRA 2012 Required Rule 10.8, Impermissible cast of composite expression
 * (different essential type categories).
 * This is required by the conversion of a bit-field of a register into int16_t type.
 *
 */

#include <stddef.h>
#include "device_registers.h"
#include "sdadc_driver.h"
#include "clock_manager.h"

/*******************************************************************************
 * Variables
 ******************************************************************************/

/* Table of base addresses for SDADC instances. */
static SDADC_Type * const s_sdadcBase[SDADC_INSTANCE_COUNT] = SDADC_BASE_PTRS;
/* The gain error variable which will be used by some functions */
static int32_t s_gainErr[FEATURE_SDADC_HAS_INSTANCE_NUMBER] = SDADC_GAIN_ERROR;
/* The offset error in case of data conversion after calibration in
   "single ended mode with negative input = VSS_HV_ADR_D" */
static int16_t s_offsetErrVss[FEATURE_SDADC_HAS_INSTANCE_NUMBER] = {0};
/* The offset error in case of data conversion after calibration in
   "differential mode" and "single ended mode with negative input =
   (VDD_HV_ADR_D – VSS_HV_ADR_D) / 2" */
static int16_t s_offsetErrVdd[FEATURE_SDADC_HAS_INSTANCE_NUMBER] = {0};

/*******************************************************************************
 * Code
 ******************************************************************************/

/*FUNCTION**********************************************************************
*
* Function Name : SDADC_DRV_GetConverterDefaultConfig
* Description   : This function initializes the members of the sdadc_conv_config_t
* structure to default values which are most commonly used for SDADC.
* This function should be called on a structure before using it to configure the converter with
* SDADC_DRV_ConfigConverter(), otherwise all members must be written by the user.
* The user can modify the desired members of the structure after calling this function.
*
* Implements    : SDADC_DRV_GetConverterDefaultConfig_Activity
* END**************************************************************************/
void SDADC_DRV_GetConverterDefaultConfig(sdadc_conv_config_t * const config)
{
    DEV_ASSERT(config != NULL);

    config->decimaRate = SDADC_DECIMATION_RATE_24;
    config->inputGain = SDADC_INPUT_GAIN_1;
    config->trigSelect = SDADC_TRIGGER_DISABLE;
    config->trigEdge = SDADC_TRIGGER_RISING_EDGE;
    config->outputSetDelay = 0xFF;
    config->highPassFilter = false;
    config->wrapAroundEnable = false;
    config->wraparound = SDADC_CHAN_AN0_AN1;
    config->channelSel = SDADC_CHAN_AN0_VREFN;
    config->enableFifo = true;
    config->fifoThreshold = 16U;
    config->stopInDebug = true;
}

/*FUNCTION**********************************************************************
*
* Function Name : SDADC_DRV_ConfigConverter
* Description   : This function configures the SDADC converter with the options
* provided in the structure.
*
* Implements    : SDADC_DRV_ConfigConverter_Activity
* END**************************************************************************/
void SDADC_DRV_ConfigConverter(const uint32_t instance,
                               const sdadc_conv_config_t * const config)
{
    DEV_ASSERT(instance < FEATURE_SDADC_HAS_INSTANCE_NUMBER);
    DEV_ASSERT(config != NULL);
    DEV_ASSERT(config->outputSetDelay >= 16u);
    DEV_ASSERT(!(config->wrapAroundEnable && (config->channelSel > config->wraparound)));

#if defined(DEV_ERROR_DETECT) || defined(CUSTOM_DEVASSERT)
    clock_names_t sdadc_clocks[FEATURE_SDADC_HAS_INSTANCE_NUMBER] = SDADC_CLOCKS;
    uint32_t sdadc_freq = 0u;
    status_t clk_status = CLOCK_SYS_GetFreq(sdadc_clocks[instance], &sdadc_freq);
    DEV_ASSERT(clk_status == STATUS_SUCCESS);
    (void) clk_status;

    DEV_ASSERT((sdadc_freq >= SDADC_CLOCK_FREQ_MIN_RUNTIME) && (sdadc_freq <= SDADC_CLOCK_FREQ_MAX_RUNTIME));
#endif

    SDADC_Type * const base = s_sdadcBase[instance];
    uint32_t mcr = 0u;
    uint32_t mode = (((uint32_t)config->channelSel & (1UL << 5U)) != 0UL) ? 1UL : 0UL;
    uint32_t vcomsel = (((uint32_t)config->channelSel & (1UL << 4U)) != 0UL) ? 1UL : 0UL;
    mcr |= SDADC_MCR_EN(1UL);
    mcr |= SDADC_MCR_MODE(mode);
    mcr |= SDADC_MCR_PDR(config->decimaRate);
    mcr |= SDADC_MCR_PGAN(config->inputGain);
    mcr |= SDADC_MCR_GECEN(0x1UL);
    mcr |= SDADC_MCR_HPFEN(config->highPassFilter);
    mcr |= SDADC_MCR_VCOMSEL(vcomsel);
    mcr |= SDADC_MCR_WRMODE(config->wrapAroundEnable);
    mcr |= SDADC_MCR_FRZ(config->stopInDebug);
    mcr |= SDADC_MCR_TRIGEN((config->trigSelect == SDADC_TRIGGER_DISABLE) ? 0UL : 1UL);
    mcr |= SDADC_MCR_TRIGEDSEL(config->trigEdge);

    /* Find the selected value that will be wrote to MCR register for trigger selection */
    if (config->trigSelect != SDADC_TRIGGER_DISABLE)
    {
        uint32_t select = (uint32_t)config->trigSelect;

        if ((uint32_t)config->trigSelect <= FEATURE_SDADC_HAS_INSTANCE_NUMBER)
        {
#ifndef FEATURE_SDADC_HAS_INSTANCE_0
            /* Calculate the selected value that be wrote to the MCR register.
               The start value of the TRIGSEL bit field of the MCR register is 0x0, but the start enum value is 0x1u,
               thus the value which will be wrote to MCR register must be the enum value minus 0x1u.*/
            select = ((uint32_t)config->trigSelect) - 0x1u;
#endif
        }
        else
        {
#ifndef FEATURE_SDADC_HAS_COMMON_TRIGGER_SELECTION
            uint32_t imcr[FEATURE_SDADC_HAS_INSTANCE_NUMBER] = TRIGGER_IMCR_REGISTER_NUMBERS;
            /* Calculate the selected value that be wrote to IMCR register.
               The start value of the SSS bit field of the IMCR register is 0x1, but the start enum value is 0x05u,
               thus the value which will be wrote to IMCR register must be the enum value minus 0x4u.*/
            select = ((uint32_t)config->trigSelect) - 0x4u;
            /* Configure IMCR register to select detail input trigger source */
            SIUL2->IMCR[imcr[instance]] = SIUL2_IMCR_SSS(select);
            /* To configure input trigger source from eTPU or eMIOS, the 0x3 value will be wrote to the TRIGSEL bit field of the MCR register */
            select = 0x3u;
#endif
        }
        mcr |= SDADC_MCR_TRIGSEL(select);
    }
    else
    {
        mcr &= ~SDADC_MCR_TRIGEN_MASK;
    }
    /* Disable SDADC */
    base->MCR &= ~SDADC_MCR_EN_MASK;
     /* Flush data fifo to make sure the fifo does not contains data of previous conversions */
    SDADC_DRV_FlushDataFifo(instance);
    /* Make sure that the data fifo overrun flag is not set */
    base->SFR = SDADC_SFR_DFFF_MASK | SDADC_SFR_DFORF_MASK | SDADC_SFR_WTHL_MASK | SDADC_SFR_WTHH_MASK;
    /* Enable SDADC */
    base->MCR |= SDADC_MCR_EN_MASK;
    /* Configure output settling delay */
    base->OSDR = SDADC_OSDR_OSD(config->outputSetDelay);
    /* Configure the input analog channel and wrap around channel */
    base->CSR = SDADC_CSR_ANCHSEL(config->channelSel) | SDADC_CSR_ANCHSEL_WRAP(config->wraparound);
    /* Configure data fifo */
    base->FCR = SDADC_FCR_FE(config->enableFifo) | SDADC_FCR_FTHLD(config->fifoThreshold);
     /* Configure MCR register */
    base->MCR |= mcr;
}

/*FUNCTION**********************************************************************
*
* Function Name : SDADC_DRV_Reset
* Description   : This function resets the SDADC internal registers to their Reference Manual reset values.
*
* Implements    : SDADC_DRV_Reset_Activity
* END**************************************************************************/
void SDADC_DRV_Reset(const uint32_t instance)
{
    DEV_ASSERT(instance < FEATURE_SDADC_HAS_INSTANCE_NUMBER);

    SDADC_Type * const base = s_sdadcBase[instance];

    base->MCR = 0u;
    base->CSR = 0u;
    base->OSDR = 0u;
    base->RSER = 0u;
    base->FCR = 0u;
    base->WTHHLR = 0u;
    /* Flush data fifo to make sure the fifo does not contains data of previous conversions */
    SDADC_DRV_FlushDataFifo(instance);
    /* Clear all status flags */
    base->SFR = SDADC_SFR_DFFF_MASK | SDADC_SFR_DFORF_MASK | SDADC_SFR_WTHL_MASK | SDADC_SFR_WTHH_MASK;
}

/*FUNCTION**********************************************************************
*
* Function Name : SDADC_DRV_Powerup
* Description   : This function disables the SDADC block. SDADC internal modulator placed in low consumption mode
*
* Implements    : SDADC_DRV_Powerdown_Activity
* END**************************************************************************/
void SDADC_DRV_Powerdown(const uint32_t instance)
{
    DEV_ASSERT(instance < FEATURE_SDADC_HAS_INSTANCE_NUMBER);

    SDADC_Type * const base = s_sdadcBase[instance];

   base->MCR &= ~SDADC_MCR_EN_MASK;
}

/*FUNCTION**********************************************************************
*
* Function Name : SDADC_DRV_Powerup
* Description   : This function enable the SDADC block.
*
* Implements    : SDADC_DRV_Powerup_Activity
* END**************************************************************************/
void SDADC_DRV_Powerup(const uint32_t instance)
{
    DEV_ASSERT(instance < FEATURE_SDADC_HAS_INSTANCE_NUMBER);


    SDADC_Type * const base = s_sdadcBase[instance];

    base->MCR |= SDADC_MCR_EN_MASK;

}

/*FUNCTION**********************************************************************
*
* Function Name : SDADC_DRV_RefreshConversion
* Description   : This function resets SDADC internal modulator to start a fresh conversion.
* This function must be call after changing converter configuration(gain, input channel, trigger, watchdog...).
*
* Implements    : SDADC_DRV_RefreshConversion_Activity
* END**************************************************************************/
void SDADC_DRV_RefreshConversion(const uint32_t instance)
{
    DEV_ASSERT(instance < FEATURE_SDADC_HAS_INSTANCE_NUMBER);

    SDADC_Type * const base = s_sdadcBase[instance];

    base->RKR = SDADC_RKR_RESET_KEY(0x5AF0u);

}

/*FUNCTION**********************************************************************
*
* Function Name : SDADC_DRV_SetInputGain
* Description   : This function configures the gain to be applied to the analog input stage of the SDADC.
*
* Implements    : SDADC_DRV_SetInputGain_Activity
* END**************************************************************************/
void SDADC_DRV_SetInputGain(const uint32_t instance,
                            const sdadc_input_gain_t gain)
{
    DEV_ASSERT(instance < FEATURE_SDADC_HAS_INSTANCE_NUMBER);

    SDADC_Type * const base = s_sdadcBase[instance];

    base->MCR &= ~SDADC_MCR_PGAN_MASK;
    base->MCR |= SDADC_MCR_PGAN(gain);

}

/*FUNCTION**********************************************************************
*
* Function Name : SDADC_DRV_SetDecimationRate
* Description   : This function configures the over-sampling ratio to be applied to support different passbands
* with a fixed input sampling clock.
*
* Implements    : SDADC_DRV_SetDecimationRate_Activity
* END**************************************************************************/
void SDADC_DRV_SetDecimationRate(const uint32_t instance,
                                 const sdadc_decimation_rate_t rate)
{
    DEV_ASSERT(instance < FEATURE_SDADC_HAS_INSTANCE_NUMBER);

    SDADC_Type * const base = s_sdadcBase[instance];

    base->MCR &= ~SDADC_MCR_PDR_MASK;
    base->MCR |= SDADC_MCR_PDR(rate);
}

/*FUNCTION**********************************************************************
*
* Function Name : SDADC_DRV_SetAnalogInputBias
* Description   : This function enables analog input bias, the analog input will be connected to
* half-scale bias(VREFP/2)
*
* Implements    : SDADC_DRV_SetAnalogInputBias_Activity
* END**************************************************************************/
void SDADC_DRV_SetAnalogInputBias(const uint32_t instance,
                                  const uint8_t inputMask,
                                  const bool enable)
{
    DEV_ASSERT(instance < FEATURE_SDADC_HAS_INSTANCE_NUMBER);

    SDADC_Type * const base = s_sdadcBase[instance];

    if (enable)
    {
        /* Enable input bias */
        base->CSR |= SDADC_CSR_BIASEN(inputMask);
    }
    else
    {
        /* Disable input bias */
        base->CSR &= ~SDADC_CSR_BIASEN(inputMask);
    }

}

/*FUNCTION**********************************************************************
*
* Function Name : SDADC_DRV_SetWatchdog
* Description   : This function configures the watch dog monitor with given parameters.
*
* Implements    : SDADC_DRV_SetWatchdog_Activity
* END**************************************************************************/
void SDADC_DRV_SetWatchdog(const uint32_t instance,
                           const bool wdgEnable,
                           const int16_t upperThreshold,
                           const int16_t lowerThreshold)
{
    DEV_ASSERT(instance < FEATURE_SDADC_HAS_INSTANCE_NUMBER);

    SDADC_Type * const base = s_sdadcBase[instance];

    if (wdgEnable)
    {
        base->MCR |= SDADC_MCR_WDGEN_MASK;
    }
    else
    {
        base->MCR &= ~SDADC_MCR_WDGEN_MASK;
    }

    base->WTHHLR = SDADC_WTHHLR_THRL(lowerThreshold) | SDADC_WTHHLR_THRH(upperThreshold);

}

/*FUNCTION**********************************************************************
*
* Function Name : SDADC_DRV_SetWraparoundMode
* Description   : This function configures the wraparound mechanism for conversion of
* programmed sequence of channels
*
* Implements    : SDADC_DRV_SetWraparoundMode_Activity
* END**************************************************************************/
void SDADC_DRV_SetWraparoundMode(const uint32_t instance,
                                 const sdadc_inputchannel_sel_t wraparound,
                                 const bool enable)
{
    DEV_ASSERT(instance < FEATURE_SDADC_HAS_INSTANCE_NUMBER);

    SDADC_Type * const base = s_sdadcBase[instance];

     /* Set wrap around value */
    base->CSR &= ~SDADC_CSR_ANCHSEL_WRAP_MASK;
    base->CSR |= SDADC_CSR_ANCHSEL_WRAP(wraparound);

    if (enable)
    {
        /* Enable wrap around mode */
        base->MCR |= SDADC_MCR_WRMODE_MASK;
    }
    else
    {
        /* Disable wrap around mode */
        base->MCR &= ~SDADC_MCR_WRMODE_MASK;
    }

}

/*FUNCTION**********************************************************************
*
* Function Name : SDADC_DRV_SelectInputChannel
* Description   : This function configures the connectivity of analog inputs to either positive or negative polarity
* terminals of the SDADC. If wraparound mode is enabled, this function supports to configure
* initial entry value for the first loop of the wraparound sequence.
*
* Implements    : SDADC_DRV_SelectInputChannel_Activity
* END**************************************************************************/
void SDADC_DRV_SelectInputChannel(const uint32_t instance,
                                  const sdadc_inputchannel_sel_t channel)
{
    DEV_ASSERT(instance < FEATURE_SDADC_HAS_INSTANCE_NUMBER);

    SDADC_Type * const base = s_sdadcBase[instance];

    base->CSR &= ~SDADC_CSR_ANCHSEL_MASK;
    base->CSR |= SDADC_CSR_ANCHSEL(channel);
}

/*FUNCTION**********************************************************************
*
* Function Name : SDADC_DRV_SwTriggerConv
* Description   : This function generates the trigger event output which can be used
* for triggering conversions.
*
* Implements    : SDADC_DRV_SwTriggerConv_Activity
* END**************************************************************************/
void SDADC_DRV_SwTriggerConv(const uint32_t instance)
{
    DEV_ASSERT(instance < FEATURE_SDADC_HAS_INSTANCE_NUMBER);

    SDADC_Type * const base = s_sdadcBase[instance];

    base->STKR = SDADC_STKR_ST_KEY(0xFFFFu);
}

/*FUNCTION**********************************************************************
*
* Function Name : SDADC_DRV_FlushDataFifo
* Description   : This function flush data fifo, all data in the fifo will be erased.
* This function is ignored if have no data in fifo.
*
* Implements    : SDADC_DRV_FlushDataFifo_Activity
* END**************************************************************************/
void SDADC_DRV_FlushDataFifo(const uint32_t instance)
{
    DEV_ASSERT(instance < FEATURE_SDADC_HAS_INSTANCE_NUMBER);

    const SDADC_Type * const base = s_sdadcBase[instance];

    bool fifoEmpty = ((base->SFR & SDADC_SFR_DFEF_MASK) != 0u) ? true : false;

    while(!fifoEmpty)
    {
        (void)base->CDR;
        fifoEmpty = ((base->SFR & SDADC_SFR_DFEF_MASK) != 0u) ? true : false;
    }
}

/*FUNCTION**********************************************************************
*
* Function Name : SDADC_DRV_GetConvDataFifo
* Description   : This function gets the converted data from the fifo and put the data into data array. The data will be consecutive popped out
* the fifo until the fifo is empty or the data array is full, so the data array length should be big enough to contain all data.
*
* Implements    : SDADC_DRV_GetConvDataFifo_Activity
* END**************************************************************************/
uint8_t SDADC_DRV_GetConvDataFifo(const uint32_t instance,
                                  const uint8_t length,
                                  int16_t * const data)
{
    DEV_ASSERT(instance < FEATURE_SDADC_HAS_INSTANCE_NUMBER);

    const SDADC_Type * const base = s_sdadcBase[instance];
    bool fifoEmpty = ((base->SFR & SDADC_SFR_DFEF_MASK) != 0u) ? true : false;
    bool validFlag = ((base->SFR & SDADC_SFR_CDVF_MASK) != 0u) ? true : false;
    uint8_t num = 0;
    int32_t rawData = 0;
    /* Get current mode and common voltage setting */
    uint32_t mode = (base->MCR & SDADC_MCR_MODE_MASK) >> SDADC_MCR_MODE_SHIFT;
    bool vcomsel = ((base->MCR & SDADC_MCR_VCOMSEL_MASK) != 0u) ? true : false;

    while(!fifoEmpty && validFlag)
    {
        rawData = (int16_t)((base->CDR & SDADC_CDR_CDATA_MASK) >> SDADC_CDR_CDATA_SHIFT);
        /* Nullify the offset error in the data conversion */
        if ((mode == 0x1UL) && !vcomsel)
        {
            rawData = rawData + s_offsetErrVss[instance];
        }
        else
        {
            rawData = rawData + s_offsetErrVdd[instance];
        }
        /* Nullify the gain error in the data conversion */
        rawData = (rawData * 65536) / s_gainErr[instance];

        if (rawData > SDADC_MAX_CONV_DATA)
        {
        	rawData = SDADC_MAX_CONV_DATA;
        }
        else if (rawData < SDADC_MIN_CONV_DATA)
        {
        	rawData = SDADC_MIN_CONV_DATA;
        }
        else
        {
        	/* Do nothing */
        }

        data[num] = (int16_t)rawData;

        fifoEmpty = ((base->SFR & SDADC_SFR_DFEF_MASK) != 0u) ? true : false;
        validFlag = ((base->SFR & SDADC_SFR_CDVF_MASK) != 0u) ? true : false;
        num++;
        if (num >= length)
        {
            break;
        }
        else
        {
            /* Do nothing */
        }
    }

    return num;
}

/*FUNCTION**********************************************************************
*
* Function Name : SDADC_DRV_GetStatusFlags
* Description   : This function returns the status flags of the SDADC.
*
* Implements    : SDADC_DRV_GetStatusFlags_Activity
* END**************************************************************************/
uint32_t SDADC_DRV_GetStatusFlags(const uint32_t instance)
{
    DEV_ASSERT(instance < FEATURE_SDADC_HAS_INSTANCE_NUMBER);

    const SDADC_Type * const base = s_sdadcBase[instance];
    uint32_t sfr = base->SFR;
    uint32_t flags = 0U;

    flags |= ((sfr & SDADC_SFR_DFEF_MASK) != 0U) ? SDADC_FLAG_DATA_FIFO_EMPTY : 0U;
    flags |= ((sfr & SDADC_SFR_WTHH_MASK) != 0U) ? SDADC_FLAG_WDG_UPPER_THRES_CROSS_OVER : 0U;
    flags |= ((sfr & SDADC_SFR_WTHL_MASK) != 0U) ? SDADC_FLAG_WDG_LOWER_THRES_CROSS_OVER : 0U;
    flags |= ((sfr & SDADC_SFR_CDVF_MASK) != 0U) ? SDADC_FLAG_CONVERTED_DATA_VALID : 0U;
    flags |= ((sfr & SDADC_SFR_DFORF_MASK) != 0U) ? SDADC_FLAG_DATA_FIFO_OVERRUN : 0U;
    flags |= ((sfr & SDADC_SFR_DFFF_MASK) != 0U) ? SDADC_FLAG_DATA_FIFO_FULL : 0U;

    return flags;

}

/*FUNCTION**********************************************************************
*
* Function Name : SDADC_DRV_ClearStatusFlags
* Description   : This function clears the status flags that are set to '1' in
* the mask.
*
* Implements    : SDADC_DRV_ClearStatusFlags_Activity
* END**************************************************************************/
void SDADC_DRV_ClearStatusFlags(const uint32_t instance,
                                const uint32_t mask)
{
    DEV_ASSERT(instance < FEATURE_SDADC_HAS_INSTANCE_NUMBER);

    SDADC_Type * const base = s_sdadcBase[instance];
    uint32_t sfr_flags = 0U;

    sfr_flags |= ((mask & SDADC_FLAG_WDG_UPPER_THRES_CROSS_OVER) != 0U) ? SDADC_SFR_WTHH(1U) : 0U;
    sfr_flags |= ((mask & SDADC_FLAG_WDG_LOWER_THRES_CROSS_OVER) != 0U) ? SDADC_SFR_WTHL(1U) : 0U;
    sfr_flags |= ((mask & SDADC_FLAG_DATA_FIFO_OVERRUN) != 0U) ? SDADC_SFR_DFORF(1U) : 0U;
    sfr_flags |= ((mask & SDADC_FLAG_DATA_FIFO_FULL) != 0U) ? SDADC_SFR_DFFF(1U) : 0U;

    /* Write-1-to-clear bits in ISR register */
    base->SFR = sfr_flags;
}

/*FUNCTION**********************************************************************
*
* Function Name : SDADC_DRV_SetGlobalDmaInterruptGate
* Description   : This function configures SDADC Global DMA/Interrupt gate.
*
* Implements    : SDADC_DRV_SetGlobalDmaInterruptGate_Activity
* END**************************************************************************/
void SDADC_DRV_SetGlobalDmaInterruptGate(const uint32_t instance,
                                         const sdadc_dmaint_gate_select_t select,
                                         const bool enable)
{
    DEV_ASSERT(instance < FEATURE_SDADC_HAS_INSTANCE_NUMBER);

    SDADC_Type * const base = s_sdadcBase[instance];

    if (enable)
    {
        /* Enable Global DMA/Interrupt gate */
        base->RSER |=  SDADC_RSER_GDIGE_MASK;
    }
    else
    {
        /* Disable Global DMA/Interrupt gate */
        base->RSER &= ~SDADC_RSER_GDIGE_MASK;
    }

#ifndef FEATURE_SDADC_HAS_COMMON_TRIGGER_SELECTION
    uint32_t imcr[FEATURE_SDADC_HAS_INSTANCE_NUMBER] = GATE_IMCR_REGISTER_NUMBERS;
    /* Configure IMCR register to select detail input gate source */
    SIUL2->IMCR[imcr[instance]] = SIUL2_IMCR_SSS(select);
#else
    uint32_t shift = instance * SIU_SDGATE_SEL_SD_A_GATE_SEL_WIDTH;
    uint32_t mask = (uint32_t)SIU_SDGATE_SEL_SD_A_GATE_SEL_MASK << shift;

    /* Configure GATE_SEL register to select detail input gate source */
    SIU->SDGATE_SEL &= ~mask;
    SIU->SDGATE_SEL |= ((uint32_t)select << shift) & mask;
#endif

}


/*FUNCTION**********************************************************************
*
* Function Name : SDADC_DRV_SetConvDataValidEvent
* Description   : This function enables/disables SDADC conversion data valid event output
*
* Implements    : SDADC_DRV_SetConvDataValidEvent_Activity
* END**************************************************************************/
void SDADC_DRV_SetConvDataValidEvent(const uint32_t instance,
                                     const bool enable)
{
    DEV_ASSERT(instance < FEATURE_SDADC_HAS_INSTANCE_NUMBER);

    SDADC_Type * const base = s_sdadcBase[instance];

    if (enable)
    {
        /* Enable conversion data valid event output*/
        base->RSER |=  SDADC_RSER_CDVEE_MASK;
    }
    else
    {
        /* Disable conversion data valid event output*/
        base->RSER &= ~SDADC_RSER_CDVEE_MASK;
    }

}

/*FUNCTION**********************************************************************
*
* Function Name : SDADC_DRV_SetFifoOverrunInterrupt
* Description   : This function enables/disables data fifo overrun interrupt generating
*
* Implements    : SDADC_DRV_SetFifoOverrunInterrupt_Activity
* END**************************************************************************/
void SDADC_DRV_SetFifoOverrunInterrupt(const uint32_t instance,
                                       const bool enable)
{
    DEV_ASSERT(instance < FEATURE_SDADC_HAS_INSTANCE_NUMBER);

    SDADC_Type * const base = s_sdadcBase[instance];

    if (enable)
    {
        /* Enable data fifo overrun interrupt */
        base->RSER |=  SDADC_RSER_DFORIE_MASK;
    }
    else
    {
        /* Disable data fifo overrun interrupt */
        base->RSER &= ~SDADC_RSER_DFORIE_MASK;
    }

}

/*FUNCTION**********************************************************************
*
* Function Name : SDADC_DRV_EnableDmaEvents
* Description   : This function enables SDADC DMA request generating.
*
* Implements    : SDADC_DRV_EnableDmaEvents_Activity
* END**************************************************************************/
void SDADC_DRV_EnableDmaEvents(const uint32_t instance,
                               const uint32_t event_mask)
{
    DEV_ASSERT(instance < FEATURE_SDADC_HAS_INSTANCE_NUMBER);

    SDADC_Type * const base = s_sdadcBase[instance];
    uint32_t rser = 0U;

    rser |= ((event_mask & SDADC_EVENT_FIFO_FULL) != 0U) ? (SDADC_RSER_DFFDIRE_MASK | SDADC_RSER_DFFDIRS_MASK) : 0U;
    rser |= ((event_mask & SDADC_EVENT_WDOG_CROSSOVER) != 0U) ? (SDADC_RSER_WTHDIRE_MASK | SDADC_RSER_WTHDIRS_MASK): 0U;

    /* Enable DMA */
    base->RSER |=  rser;

}

/*FUNCTION**********************************************************************
*
* Function Name : SDADC_DRV_EnableInterruptEvents
* Description   : This function enables SDADC interrupt request generating.
*
* Implements    : SDADC_DRV_EnableInterruptEvents_Activity
* END**************************************************************************/
void SDADC_DRV_EnableInterruptEvents(const uint32_t instance,
                                     const uint32_t event_mask)
{
    DEV_ASSERT(instance < FEATURE_SDADC_HAS_INSTANCE_NUMBER);

    SDADC_Type * const base = s_sdadcBase[instance];
    uint32_t rser = 0U;

    rser |= ((event_mask & SDADC_EVENT_FIFO_FULL) != 0U) ? SDADC_RSER_DFFDIRE_MASK : 0U;
    rser |= ((event_mask & SDADC_EVENT_WDOG_CROSSOVER) != 0U) ? SDADC_RSER_WTHDIRE_MASK : 0U;

    /* Enable interrupt */
    base->RSER &= ~(SDADC_RSER_DFFDIRS_MASK | SDADC_RSER_WTHDIRS_MASK);
    base->RSER |=  rser;

}

/*FUNCTION**********************************************************************
*
* Function Name : SDADC_DRV_DisableEvents
* Description   : This function disable SDADC DMA and interrupt request generating
*
* Implements    : SDADC_DRV_DisableEvents_Activity
* END**************************************************************************/
void SDADC_DRV_DisableEvents(const uint32_t instance,
                             const uint32_t event_mask)
{
    DEV_ASSERT(instance < FEATURE_SDADC_HAS_INSTANCE_NUMBER);

    SDADC_Type * const base = s_sdadcBase[instance];

    /* Enable interrupt */
    if ((event_mask & SDADC_EVENT_FIFO_FULL) != 0U)
    {
        base->RSER &= ~SDADC_RSER_DFFDIRE_MASK;
    }
    else
    {
        /* Do nothing*/
    }

    if ((event_mask & SDADC_EVENT_WDOG_CROSSOVER) != 0U)
    {
        base->RSER &= ~SDADC_RSER_WTHDIRE_MASK;
    }
    else
    {
        /* Do nothing*/
    }
}

/*FUNCTION**********************************************************************
*
* Function Name : SDADC_DRV_GetInterruptNumber
* Description   : This function returns the interrupt number for the specified SDADC instance.
*
* Implements    : SDADC_DRV_GetInterruptNumber_Activity
* END**************************************************************************/
IRQn_Type SDADC_DRV_GetInterruptNumber(const uint32_t instance)
{
    DEV_ASSERT(instance < FEATURE_SDADC_HAS_INSTANCE_NUMBER);

    const IRQn_Type sdadcIrqId[FEATURE_SDADC_HAS_INSTANCE_NUMBER] = SDADC_IRQS;
    IRQn_Type irqId = sdadcIrqId[instance];

    return irqId;
}

/*FUNCTION**********************************************************************
*
* Function Name : SDADC_DRV_GainCalibration
* Description   : This function performs a gain calibration of the SDADC. Gain calibration
* should be run before using the SDADC converter or after the operating conditions
* (particularly Vref) change significantly. The measured gain value is going be used to
* nullify the gain errors in the data conversion.
* The conversion number that is performed by calibration should be from 16 to 64. The higher the number
* of conversion done, the higher the rejection of noise during the calibration.
*
* Implements    : SDADC_DRV_GainCalibration_Activity
* END**************************************************************************/
void SDADC_DRV_GainCalibration(const uint32_t instance,
                               const uint8_t convNum)
{
    DEV_ASSERT(instance < FEATURE_SDADC_HAS_INSTANCE_NUMBER);

#if defined(DEV_ERROR_DETECT) || defined(CUSTOM_DEVASSERT)
    clock_names_t sdadc_clocks[FEATURE_SDADC_HAS_INSTANCE_NUMBER] = SDADC_CLOCKS;
    uint32_t sdadc_freq = 0u;
    status_t clk_status = CLOCK_SYS_GetFreq(sdadc_clocks[instance], &sdadc_freq);
    DEV_ASSERT(clk_status == STATUS_SUCCESS);
    (void) clk_status;
    DEV_ASSERT((sdadc_freq >= SDADC_CLOCK_FREQ_MIN_RUNTIME) && (sdadc_freq <= SDADC_CLOCK_FREQ_MAX_RUNTIME));
#endif

    SDADC_Type * const base = s_sdadcBase[instance];
    int32_t actualData = 0;
    int32_t dP = 0;
    int32_t dN = 0;
    uint8_t num = convNum;
    uint8_t i;
    int32_t tmp = 0;
    /* The conversion number should be from 16 to 64 */
    if (convNum < 16u)
    {
        num = 16u;
    }
    else if (convNum > 64u)
    {
        num = 64u;
    }
    else
    {
        /* Do nothing */
    }

    /* Save the current state of MCR to restore the configuration after calibration */
    uint32_t currentMCR = base->MCR;
    /* Save the current state of CSR to restore the configuration after calibration */
    uint32_t currentCSR = base->CSR;
    /* Save the current state of OSDR to restore the configuration after calibration */
    uint32_t currentOSDR = base->OSDR;
    /* Save the current state of FCR to restore the configuration after calibration */
    uint32_t currentFCR = base->FCR;

    base->MCR &= ~SDADC_MCR_EN_MASK;
    /* Flush data fifo to make sure the fifo does not contains data of previous conversions */
    SDADC_DRV_FlushDataFifo(instance);
    /* Make sure that the data fifo overrun flag is not set */
    SDADC_DRV_ClearStatusFlags(instance, (SDADC_FLAG_DATA_FIFO_OVERRUN | SDADC_FLAG_DATA_FIFO_FULL));
    /* Enable SDADC */
    base->MCR |= SDADC_MCR_EN_MASK;
    /* Configure converter to perform gain calibration */
    base->MCR = SDADC_MCR_EN(1u) | SDADC_MCR_MODE(0u) | SDADC_MCR_GECEN(1u) | SDADC_MCR_HPFEN(0u) | SDADC_MCR_PGAN(0u);
    /* Output settling Time delay has to be set at least to 16 */
    base->OSDR = SDADC_OSDR_OSD(0xFFu);
    /* Select input channel mux to perform full positive scale calibration */
    base->CSR = SDADC_CSR_BIASEN(0u) | SDADC_CSR_ANCHSEL(0x6u);
    /* Enable the data fifo  */
    base->FCR = SDADC_FCR_FE(1u) | SDADC_FCR_FTHLD(15u);
    /* Start calibration */
    base->RKR = SDADC_RKR_RESET_KEY(0x5AF0u);

    for (i = 0u; i < num; i++)
    {
        /* Wait for conversion to finish */
        while ((base->SFR & SDADC_SFR_DFEF_MASK) != 0u)
        {}
        tmp = (int16_t)((base->CDR & SDADC_CDR_CDATA_MASK) >> SDADC_CDR_CDATA_SHIFT);
        actualData = actualData + tmp;
    }
    /* Calculate average of full positive scale data conversions */
    dP = (int32_t)(actualData / num);

    /* Perform full negative scale calibration */
    base->MCR &= ~SDADC_MCR_EN_MASK;
    /* Flush data fifo to make sure the fifo does not contains data of previous conversions */
    SDADC_DRV_FlushDataFifo(instance);
     /* Make sure that the data fifo overrun flag is not set */
    SDADC_DRV_ClearStatusFlags(instance, (SDADC_FLAG_DATA_FIFO_OVERRUN | SDADC_FLAG_DATA_FIFO_FULL));
     /* Enable SDADC */
    base->MCR |= SDADC_MCR_EN_MASK;
    /* Change the input channel mux to perform full negative scale calibration */
    base->CSR = SDADC_CSR_ANCHSEL(0x7u);
    /* Start calibration */
    base->RKR = SDADC_RKR_RESET_KEY(0x5AF0u);
    actualData = 0u;
    for (i = 0u; i < num; i++)
    {
        /* Wait for conversion to finish */
        while ((base->SFR & SDADC_SFR_DFEF_MASK) != 0u)
        {}
        tmp = (int16_t)((base->CDR & SDADC_CDR_CDATA_MASK) >> SDADC_CDR_CDATA_SHIFT);
        actualData = actualData + tmp;
    }
    /* Stop conversion */
    base->MCR &= ~SDADC_MCR_EN_MASK;
    /* Calculate average of full negative scale data conversions */
    dN = (int32_t)(actualData / num);
    /* Calculate the gain error */
    s_gainErr[instance] = dP - dN;

    /* Restore the configuration of OSDR register */
    base->OSDR = currentOSDR;
    /* Restore the configuration of CSR register */
    base->CSR = currentCSR;
    /* Restore the configuration of FCR register */
    base->FCR = currentFCR;
    /* Restore the configuration of MCR register */
    base->MCR = currentMCR;
}

/*FUNCTION**********************************************************************
*
* Function Name : SDADC_DRV_OffsetCalibration
* Description   : This function performs a offset calibration of the SDADC. Offset calibration
* should be run before using the SDADC converter or after the operating conditions
* (particularly Vref, input gain) change significantly.
* The measured offset is going be used to nullify the offset error in the data conversion.
* The offset calibration must be performed for each input gain changing since it is expected to
* vary with input gain configuration of SDADC.
*
* Implements    : ASDADC_DRV_OffsetCalibration_Activity
* END**************************************************************************/
void SDADC_DRV_OffsetCalibration(const uint32_t instance,
                                 const sdadc_input_gain_t userGain)
{
    DEV_ASSERT(instance < FEATURE_SDADC_HAS_INSTANCE_NUMBER);

#if defined(DEV_ERROR_DETECT) || defined(CUSTOM_DEVASSERT)
    clock_names_t sdadc_clocks[FEATURE_SDADC_HAS_INSTANCE_NUMBER] = SDADC_CLOCKS;
    uint32_t sdadc_freq = 0u;
    status_t clk_status = CLOCK_SYS_GetFreq(sdadc_clocks[instance], &sdadc_freq);
    DEV_ASSERT(clk_status == STATUS_SUCCESS);
    (void) clk_status;
    DEV_ASSERT((sdadc_freq >= SDADC_CLOCK_FREQ_MIN_RUNTIME) && (sdadc_freq <= SDADC_CLOCK_FREQ_MAX_RUNTIME));
#endif

    SDADC_Type * const base = s_sdadcBase[instance];
    int16_t actualData = 0;
    int16_t expectedData = 0;

    /* Save the current state of MCR to restore the configuration after calibration */
    uint32_t currentMCR = base->MCR;
    /* Save the current state of CSR to restore the configuration after calibration */
    uint32_t currentCSR = base->CSR;
    /* Save the current state of OSDR to restore the configuration after calibration */
    uint32_t currentOSDR = base->OSDR;
    /* Configure converter to perform gain calibration */
    base->MCR &= ~SDADC_MCR_EN_MASK;
    /* Flush data fifo to make sure the fifo does not contains data of previous conversions */
    SDADC_DRV_FlushDataFifo(instance);
    /* Make sure that the data fifo overrun flag is not set */
    SDADC_DRV_ClearStatusFlags(instance, (SDADC_FLAG_DATA_FIFO_OVERRUN | SDADC_FLAG_DATA_FIFO_FULL));
    /* Enable SDADC */
    base->MCR |= SDADC_MCR_EN_MASK;
    /* Configure SDADC converter */
    base->MCR = SDADC_MCR_EN(1u) | SDADC_MCR_MODE(0u) | SDADC_MCR_GECEN(1u) | SDADC_MCR_HPFEN(0u) | SDADC_MCR_PGAN(userGain);
    /* Output settling Time delay has to be set at least to 16 */
    base->OSDR = SDADC_OSDR_OSD(0xFFu);
    /* Perform calibration in case of data conversion after calibration in "single ended mode with negative input = VSS_HV_ADR_D" */
    base->CSR = SDADC_CSR_BIASEN(0u) | SDADC_CSR_ANCHSEL(0x4u);
    /* Start calibration */
    base->RKR = SDADC_RKR_RESET_KEY(0x5AF0u);

    while ((base->SFR & SDADC_SFR_DFEF_MASK) != 0u)
    {
        /* Wait for conversion to finish */
    }
    /* Get the conversion data */
    actualData = (int16_t)((base->CDR & SDADC_CDR_CDATA_MASK) >> SDADC_CDR_CDATA_SHIFT);
    /* Calculate the offset error */
    s_offsetErrVss[instance] = expectedData - actualData;

    /* Disable SDADC */
    base->MCR &= ~SDADC_MCR_EN_MASK;
    /* Flush data fifo to make sure the fifo does not contains data of previous conversions */
    SDADC_DRV_FlushDataFifo(instance);
    /* Make sure that the data fifo overrun flag is not set */
    SDADC_DRV_ClearStatusFlags(instance, (SDADC_FLAG_DATA_FIFO_OVERRUN | SDADC_FLAG_DATA_FIFO_FULL));
    /* Enable SDADC */
    base->MCR |= SDADC_MCR_EN_MASK;
    /* Perform calibration in case of data conversion after calibration in "differential mode" and
    "single ended mode with negative input = (VDD_HV_ADR_D – VSS_HV_ADR_D) / 2" */
    base->CSR = SDADC_CSR_ANCHSEL(0x5u);
    /* Start calibration */
    base->RKR = SDADC_RKR_RESET_KEY(0x5AF0u);
    actualData = 0;

    while ((base->SFR & SDADC_SFR_DFEF_MASK) != 0u)
    {
        /* Wait for conversion to finish */
    }
    /* Get the conversion data */
    actualData = (int16_t)((base->CDR & SDADC_CDR_CDATA_MASK) >> SDADC_CDR_CDATA_SHIFT);
    /* Calculate the offset error */
    s_offsetErrVdd[instance] = expectedData - actualData;
    /* Stop conversion */
    base->MCR &= ~SDADC_MCR_EN_MASK;
    /* Restore the configuration of OSDR register */
    base->OSDR = currentOSDR;
    /* Restore the configuration of CSR register */
    base->CSR = currentCSR;
    /* Restore the configuration of MCR register */
    base->MCR = currentMCR;

}

/*******************************************************************************
 * EOF
 ******************************************************************************/
